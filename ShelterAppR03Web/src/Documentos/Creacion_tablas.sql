/*CREACI�N DE Usuario*/

CREATE TABLESPACE ShelterApp
DATAFILE 'c:/ShelterApp.dat' SIZE 2048M
AUTOEXTEND ON NEXT 10M
MAXSIZE 2048M;
CREATE USER ShelterApp
IDENTIFIED BY root
DEFAULT TABLESPACE ShelterApp
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK
PROFILE DEFAULT;
GRANT CONNECT TO ShelterApp;
GRANT RESOURCE TO ShelterApp;



/*CREACI�N DE TABLAS*/

create sequence di_seq1
start with 1
increment by 1;
CREATE TABLE PERRERA(
DI int,
CIF varchar2(10) NOT NULL,
NOMBRE varchar2(30) NOT NULL,
DIRECCION varchar2(100),
TELEFONO int,
EMAIL varchar2(30) NOT NULL,
LOCALIDAD varchar2(30) NOT NULL,
PROVINCIA varchar2(20),
NUMERO_CUENTA varchar2(20) NOT NULL,
PASS varchar2(25) NOT NULL,
constraint per_cif_pk primary key(cif)
)

create sequence di_seq3
start with 1
increment by 1;
CREATE TABLE VETERINARIO(
DI int,
DNI varchar2(10) NOT NULL ,
CIF_PERRERA varchar2(10) NOT NULL,
NOMBRE varchar2(20) NOT NULL,
APELLIDO1 varchar2(20),
APELLIDO2 varchar2(20),
constraint vet_dni_pk primary key(dni),
constraint vet_cif_fk foreign key(cif_perrera) references perrera(cif)
)

create sequence di_seq4
start with 1
increment by 1;
CREATE TABLE MASCOTAS(
DI int,
CODIGO varchar2(10) NOT NULL,
CODIGO_PERRERA varchar2(10) NOT NULL,
DNI_VETERINARIO varchar2(10)NOT NULL,
TIPO_MASCOTA varchar2(5) check(TIPO_MASCOTA='GATO' OR TIPO_MASCOTA='PERRO')NOT NULL,
NOMBRE varchar2(10),
RAZA varchar2(20),
TAMA�O varchar2(10),
SEXO varchar2(1) check(SEXO='M' OR SEXO='H'),
DNI_DUE�O varchar2(10),
SALUD varchar2(20),
COLOR varchar2(10),
FOTO varchar2(20),
constraint mas_cod_pk primary key(codigo),
constraint mas_dnivet_fk foreign key(dni_veterinario) references veterinario(dni)
)

create sequence di_seq5
start with 1
increment by 1;
CREATE TABLE INFORMACI�N(
DI int,
CODIGO varchar2(10) NOT NULL,
CIF_PERRERA varchar2(10),
NOMBRE_USUARIO varchar2(10),
TIPO_INFORMACION VARCHAR2(10)NOT NULL,
FECHA date,
CONTENIDO varchar2(200) NOT NULL,
constraint inf_cod_pk primary key(codigo),
constraint inf_cif_fk foreign key(cif_perrera) references perrera(cif)
)

create sequence di_seq6
start with 1
increment by 1;
CREATE TABLE APADRINA(
di int,
DNI varchar2(10) NOT NULL,
CODIGO_MASCOTA varchar2(10) NOT NULL unique,
IMPORTE int NOT NULL,
constraint apa_cod_fk foreign key(codigo_mascota) references mascotas(codigo)
)

create sequence di_seq7
start with 1
increment by 1;
CREATE TABLE ACOGIDA(
di int,
CODIGO_MASCOTA varchar2(10) NOT NULL unique,
DNI varchar2(10) NOT NULL,
TIEMPO varchar2(20),
constraint aco_cod_fk foreign key (codigo_mascota) references mascotas(codigo)
)

/*Eliminar tablas*/
drop table acogida;
drop table apadrina;
drop table informaci�n;
drop table mascotas;
drop table veterinario;
drop table perrera;