package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.*;
import interficies.Adopciones.*;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Toolkit;

public class ModificarAdopciones extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private JTextField textField;
	private JLabel lblIdAnimal;
	private JLabel lblIdAdoptante;
	private JLabel label;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public ModificarAdopciones() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ModificarVeterinarios.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(500, 200, 509, 202);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(ModificarAdopciones.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);
		
		textField_2 = new JTextField();
		textField_2.setBounds(309, 75, 170, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnNewButton = new JButton("A\u00F1adir/Editar");
		btnNewButton.setBounds(165, 105, 113, 20);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Eliminar");
		btnNewButton_1.setBounds(165, 136, 113, 20);
		contentPane.add(btnNewButton_1);
		
		textField = new JTextField();
		textField.setBounds(309, 44, 170, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblIdAnimal = new JLabel("ID Animal");
		lblIdAnimal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIdAnimal.setBounds(165, 43, 93, 20);
		contentPane.add(lblIdAnimal);
		
		lblIdAdoptante = new JLabel("ID Adoptante");
		lblIdAdoptante.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIdAdoptante.setBounds(165, 74, 113, 20);
		contentPane.add(lblIdAdoptante);
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(ModificarAdopciones.class.getResource("/Documentos/Fondo.jpg")));
		label.setBounds(0, 0, 509, 180);
		contentPane.add(label);
		
		//EVENTOS
		//A�ADIR/EDITAR
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 SQL db = new SQL();
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.editAdopciones(Integer.parseInt(textField.getText()),textField_2.getText());
					 db.closeConnection();
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
			}
		});
		//ELIMINAR
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.eliminaAdopcion(Integer.parseInt(textField.getText()));
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
}
