package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import SQL.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Font;

public class TodoslosAnimales extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private String[][] animales = new String[9][12];
	/**
	 * Create the frame.
	 */
	public TodoslosAnimales() {
		this.animales[0][0]="CODIGO";
		this.animales[0][1]="CODIGO_PERRERA";
		this.animales[0][2]="DNI_VETERINARIO";
		this.animales[0][3]="TIPO_MASCOTA";
		this.animales[0][4]="NOMBRE";
		this.animales[0][5]="RAZA";
		this.animales[0][6]="TAMA�O";
		this.animales[0][7]="SEXO";
		this.animales[0][8]="DNI_DUE�O";
		this.animales[0][9]="SALUD";
		this.animales[0][10]="COLOR";
		this.animales[0][11]="FOTO";
		for (int i=1;i<animales.length;i++){
			for(int j=0;j<11;j++){
				this.animales[i][j]="";
			}
		}
		setIconImage(Toolkit.getDefaultToolkit().getImage(Animales.class.getResource("/Documentos/Huella.png")));
		setBackground(new Color(51, 51, 51));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(500, 200, 689, 384);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		// Logo de la empresa
		JLabel lblLogo = new JLabel("LOGO");
		lblLogo.setIcon(new ImageIcon(Acogidas.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);
		//Botón Añadir animales
		JButton btnNewButton = new JButton("A\u00F1adir/Editar");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(297, 293, 151, 20);
		contentPane.add(btnNewButton);
		//Botón Editar animales
		JButton btnNewButton_1 = new JButton("Actualizar");
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setBounds(298, 322, 150, 20);
		contentPane.add(btnNewButton_1);
		//Tabla informativa de los animales
		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{animales[0][0],animales[0][1],animales[0][2], animales[0][3],animales[0][4],animales[0][5], animales[0][6],animales[0][7],animales[0][8], animales[0][9], animales[0][10], animales[0][11]},
					{animales[1][0],animales[1][1],animales[1][2], animales[1][3],animales[1][4],animales[1][5], animales[1][6],animales[1][7],animales[1][8], animales[1][9], animales[1][10], animales[1][11]},
					{animales[2][0],animales[2][1],animales[2][2], animales[2][3],animales[2][4],animales[2][5], animales[2][6],animales[2][7],animales[2][8], animales[2][9], animales[2][10], animales[2][11]},
					{animales[3][0],animales[3][1],animales[3][2], animales[3][3],animales[3][4],animales[3][5], animales[3][6],animales[3][7],animales[3][8], animales[3][9], animales[3][10], animales[3][11]},
					{animales[4][0],animales[4][1],animales[4][2], animales[4][3],animales[4][4],animales[4][5], animales[4][6],animales[4][7],animales[4][8], animales[4][9], animales[4][10], animales[4][11]},
					{animales[5][0],animales[5][1],animales[5][2], animales[5][3],animales[5][4],animales[5][5], animales[5][6],animales[5][7],animales[5][8], animales[5][9], animales[5][10], animales[5][11]},
					{animales[6][0],animales[6][1],animales[6][2], animales[6][3],animales[6][4],animales[6][5], animales[6][6],animales[6][7],animales[6][8], animales[6][9], animales[6][10], animales[6][11]},
					{animales[7][0],animales[7][1],animales[7][2], animales[7][3],animales[7][4],animales[7][5], animales[7][6],animales[7][7],animales[7][8], animales[7][9], animales[7][10], animales[7][11]},
					{animales[8][0],animales[8][1],animales[8][2], animales[8][3],animales[8][4],animales[8][5], animales[8][6],animales[8][7],animales[8][8], animales[8][9], animales[8][10], animales[8][11]},
				},
				new String[] {
					"CODIGO","CODIGO_PERRERA","DNI_VETERINARIO","TIPO_MASCOTA","NOMBRE","RAZA","TAMA�O","SEXO","DNI_DUE�O","SALUD","COLOR" ,"FOTO"
				}
			));
		table.setBounds(50, 137, 621, 144);
		contentPane.add(table);
		
		JLabel lblTodosLosAnimales = new JLabel("Todos los animales");
		lblTodosLosAnimales.setFont(new Font("Corbel", Font.BOLD, 22));
		lblTodosLosAnimales.setBounds(252, 57, 290, 40);
		contentPane.add(lblTodosLosAnimales);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon(TodoslosAnimales.class.getResource("/Documentos/Fondo.jpg")));
		lblFondo.setBounds(0, 0, 689, 362);
		contentPane.add(lblFondo);
		//EVENTOS
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarTodosLosAnimales ma=new ModificarTodosLosAnimales();
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					animales=db.getValues2();
					anadirAnimal(animales,table);
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
		//METODOS
		public void anadirAnimal(String[][] array,JTable table){
			table.setModel(new DefaultTableModel(
					new Object[][] {
						{animales[0][0],animales[0][1],animales[0][2], animales[0][3],animales[0][4],animales[0][5], animales[0][6],animales[0][7],animales[0][8], animales[0][9], animales[0][10], animales[0][11]},
						{animales[1][0],animales[1][1],animales[1][2], animales[1][3],animales[1][4],animales[1][5], animales[1][6],animales[1][7],animales[1][8], animales[1][9], animales[1][10], animales[1][11]},
						{animales[2][0],animales[2][1],animales[2][2], animales[2][3],animales[2][4],animales[2][5], animales[2][6],animales[2][7],animales[2][8], animales[2][9], animales[2][10], animales[2][11]},
						{animales[3][0],animales[3][1],animales[3][2], animales[3][3],animales[3][4],animales[3][5], animales[3][6],animales[3][7],animales[3][8], animales[3][9], animales[3][10], animales[3][11]},
						{animales[4][0],animales[4][1],animales[4][2], animales[4][3],animales[4][4],animales[4][5], animales[4][6],animales[4][7],animales[4][8], animales[4][9], animales[4][10], animales[4][11]},
						{animales[5][0],animales[5][1],animales[5][2], animales[5][3],animales[5][4],animales[5][5], animales[5][6],animales[5][7],animales[5][8], animales[5][9], animales[5][10], animales[5][11]},
						{animales[6][0],animales[6][1],animales[6][2], animales[6][3],animales[6][4],animales[6][5], animales[6][6],animales[6][7],animales[6][8], animales[6][9], animales[6][10], animales[6][11]},
						{animales[7][0],animales[7][1],animales[7][2], animales[7][3],animales[7][4],animales[7][5], animales[7][6],animales[7][7],animales[7][8], animales[7][9], animales[7][10], animales[7][11]},
						{animales[8][0],animales[8][1],animales[8][2], animales[8][3],animales[8][4],animales[8][5], animales[8][6],animales[8][7],animales[8][8], animales[8][9], animales[8][10], animales[8][11]},
					},
					new String[] {
							"Tipo", "Nombre", "Raza", "Tama�o", "Edad", "Due�o", "Salud", "Color", "Sexo", "Codigo_Perrera", "DNI_Veterinario"
					}
				));
		}
		public void eliminarAnimal(String[][] array,JTable table){
				int a=8;
					for(int i=1;i<array.length;i++){
						for(int j=0;i<11;j++){
							if(array[i][j].equals("")){
								a=i;
							}
						}
					}
					animales[a][0]="";
					animales[a][1]="";
					animales[a][2]="";
					animales[a][3]="";
					animales[a][4]="";
					animales[a][5]="";
					animales[a][6]="";
					animales[a][7]="";
					animales[a][8]="";
					animales[a][9]="";
					animales[a][10]="";	
			table.setModel(new DefaultTableModel(
					new Object[][] {
						{animales[0][0],animales[0][1],animales[0][2], animales[0][3],animales[0][4],animales[0][5], animales[0][6],animales[0][7],animales[0][8], animales[0][9], animales[0][10]},
						{animales[1][0],animales[1][1],animales[1][2], animales[1][3],animales[1][4],animales[1][5], animales[1][6],animales[1][7],animales[1][8], animales[0][9], animales[0][10]},
						{animales[2][0],animales[2][1],animales[2][2], animales[2][3],animales[2][4],animales[2][5], animales[2][6],animales[2][7],animales[2][8], animales[0][9], animales[0][10]},
						{animales[3][0],animales[3][1],animales[3][2], animales[3][3],animales[3][4],animales[3][5], animales[3][6],animales[3][7],animales[3][8], animales[0][9], animales[0][10]},
						{animales[4][0],animales[4][1],animales[4][2], animales[4][3],animales[4][4],animales[4][5], animales[4][6],animales[4][7],animales[4][8], animales[0][9], animales[0][10]},
						{animales[5][0],animales[5][1],animales[5][2], animales[5][3],animales[5][4],animales[5][5], animales[5][6],animales[5][7],animales[5][8], animales[0][9], animales[0][10]},
						{animales[6][0],animales[6][1],animales[6][2], animales[6][3],animales[6][4],animales[6][5], animales[6][6],animales[6][7],animales[6][8], animales[0][9], animales[0][10]},
						{animales[7][0],animales[7][1],animales[7][2], animales[7][3],animales[7][4],animales[7][5], animales[7][6],animales[7][7],animales[7][8], animales[0][9], animales[0][10]},
						{animales[8][0],animales[8][1],animales[8][2], animales[8][3],animales[8][4],animales[8][5], animales[8][6],animales[8][7],animales[8][8], animales[0][9], animales[0][10]},
					},
					new String[] {
							"Tipo", "Nombre", "Raza", "Tama�o", "Edad", "Due�o", "Salud", "Color", "Sexo", "Codigo_Perrera", "DNI_Veterinario"
					}
				));
		}
		public JTable getTable5() {
			return table;
		}
		public void setTable(JTable table) {
			this.table = table;
		}
		public String[][] getAnimales() {
			return animales;
		}
		public void setAdopciones(String[][] animales) {
			this.animales = animales;
		}
		public JTable getTable() {
			return table;
		}
		public void setAnimales(String[][] animales) {
			this.animales = animales;
		}
		
	}
