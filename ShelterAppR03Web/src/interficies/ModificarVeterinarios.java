package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SQL;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.Font;

public class ModificarVeterinarios extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtrApellido;
	private JTextField txtnApellido;
	private JTextField txtDni;
	private JTextField txtCifProtectora;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public ModificarVeterinarios(String DNI, String CIF, String NOMBRE, String Ap1, String Ap2) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ModificarVeterinarios.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(500, 200, 509, 290);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		// Campo insertar nombre
		txtNombre = new JTextField();
		txtNombre.setBounds(309, 44, 170, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		txtNombre.setText(NOMBRE);
		// Campo insertar 1r Apellido
		txtrApellido = new JTextField();
		txtrApellido.setBounds(309, 75, 170, 20);
		contentPane.add(txtrApellido);
		txtrApellido.setColumns(10);
		txtrApellido.setText(Ap1);
		// Campo insertar 2n Apellido
		txtnApellido = new JTextField();
		txtnApellido.setBounds(309, 106, 170, 20);
		contentPane.add(txtnApellido);
		txtnApellido.setColumns(10);
		txtnApellido.setText(Ap2);
		// Campo insertar DNI
		txtDni = new JTextField();
		txtDni.setBounds(309, 137, 170, 20);
		contentPane.add(txtDni);
		txtDni.setColumns(10);
		txtDni.setText(DNI);
		// Campo insertar CIF Protectora
		txtCifProtectora = new JTextField();
		txtCifProtectora.setBounds(309, 168, 170, 20);
		contentPane.add(txtCifProtectora);
		txtCifProtectora.setColumns(10);
		txtCifProtectora.setText(CIF);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(165, 42, 65, 20);
		contentPane.add(lblNombre);
		
		JLabel lblrApellido = new JLabel("1r Apellido");
		lblrApellido.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblrApellido.setBounds(165, 73, 86, 20);
		contentPane.add(lblrApellido);
		
		JLabel lblnApellido = new JLabel("2n Apellido");
		lblnApellido.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblnApellido.setBounds(165, 104, 73, 20);
		contentPane.add(lblnApellido);
		
		JLabel lblDni = new JLabel("DNI");
		lblDni.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDni.setBounds(165, 135, 46, 20);
		contentPane.add(lblDni);
		
		JLabel lblCifProtectora = new JLabel("CIF Protectora");
		lblCifProtectora.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCifProtectora.setBounds(165, 166, 106, 20);
		contentPane.add(lblCifProtectora);
		
		JButton btneditar = new JButton("Editar");
		btneditar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btneditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btneditar.setBounds(165, 228, 113, 20);
		contentPane.add(btneditar);
		
		JLabel lblNewLabel = new JLabel("ID");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(166, 197, 46, 20);
		contentPane.add(lblNewLabel);
		
		// Campo ID para indexar
		textField = new JTextField();
		textField.setBounds(309, 200, 170, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ModificarVeterinarios.class.getResource("/Documentos/Logo.png")));
		label.setBounds(50, 11, 81, 108);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(ModificarVeterinarios.class.getResource("/Documentos/Fondo.jpg")));
		label_1.setBounds(0, 0, 509, 309);
		contentPane.add(label_1);
		//EDITAR
		btneditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 SQL db = new SQL();
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.editSTRINGtesting("VETERINARIO", txtDni.getText(), "DNI", txtDni.getText());
					db.editSTRINGtesting("VETERINARIO", txtDni.getText(), "NOMBRE", txtNombre.getText());
					db.editSTRINGtesting("VETERINARIO", txtDni.getText(), "APELLIDO1", txtrApellido.getText());
					db.editSTRINGtesting("VETERINARIO", txtDni.getText(), "APELLIDO2", txtnApellido.getText());
					// db.editSTRINGRecord("VETERINARIO", Integer.parseInt(textField.getText()), "NOMBRE", txtNombre.getText());
					// db.editSTRINGRecord("VETERINARIO", Integer.parseInt(textField.getText()), "APELLIDO1", txtrApellido.getText());
					// db.editSTRINGRecord("VETERINARIO", Integer.parseInt(textField.getText()), "APELLIDO2", txtnApellido.getText());
					 //db.editSTRINGRecord("VETERINARIO", Integer.parseInt(textField.getText()), "CIF_PERRERA", txtCifProtectora.getText());
					 db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
			}
		});
	}
}
