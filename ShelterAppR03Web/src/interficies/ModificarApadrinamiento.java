package interficies;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.*;


import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;

public class ModificarApadrinamiento extends JFrame {

	private JPanel contentPane;
	private JTextField textField_CA;
	private JTextField textField_DNI;
	private JTextField textField_Imp;

	

	/**
	 * Launch the application.
	 */
	
	

	/**
	 * Create the frame.
	 */
	public ModificarApadrinamiento(String DNI, String CODIGO, String IMPORTE) {
		setTitle("ShelterApp");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ModificarApadrinamiento.class.getResource("/Documentos/Huella.png")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(500, 200, 509, 197);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(165, 136, 113, 20);
		contentPane.add(btnAceptar);
		
		JLabel lblCdigoDelAnimal = new JLabel("C\u00F3digo del animal");
		lblCdigoDelAnimal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCdigoDelAnimal.setBounds(165, 43, 134, 20);
		contentPane.add(lblCdigoDelAnimal);
		
		JLabel lblDniPadrino = new JLabel("DNI Padrino");
		lblDniPadrino.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDniPadrino.setBounds(165, 74, 110, 20);
		contentPane.add(lblDniPadrino);
		
		textField_CA = new JTextField();
		textField_CA.setBounds(309, 44, 170, 20);
		textField_CA.setEditable(false);
		contentPane.add(textField_CA);
		textField_CA.setColumns(10);
		textField_CA.setText(CODIGO);
		
		textField_DNI = new JTextField();
		textField_DNI.setColumns(10);
		textField_DNI.setBounds(309, 75, 170, 20);
		contentPane.add(textField_DNI);
		textField_DNI.setText(DNI);
		

		textField_Imp = new JTextField();
		textField_Imp.setColumns(10);
		textField_Imp.setBounds(309, 106, 170, 20);
		contentPane.add(textField_Imp);
		textField_Imp.setText(IMPORTE);
		
		
		JLabel label = new JLabel("Importe");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(165, 105, 71, 20);
		contentPane.add(label);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(ModificarApadrinamiento.class.getResource("/Documentos/Logo.png")));
		lblNewLabel.setBounds(50, 11, 81, 108);
		contentPane.add(lblNewLabel);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(ModificarApadrinamiento.class.getResource("/Documentos/Fondo.jpg")));
		label_1.setBounds(0, 0, 509, 175);
		contentPane.add(label_1);
		
		
		//Eventos
		//Aceptar
		
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 SQL db = new SQL();
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.editINTRecordMarta("APADRINA", textField_CA.getText(), "IMPORTE", textField_Imp.getText());
					db.editSTRINGRecordMarta("APADRINA", textField_CA.getText(), "DNI", textField_DNI.getText());
					db.closeConnection();
					setVisible(false);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
	}
}
				
				

