package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import SQL.SQL;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class Adopciones extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private String[][] adopciones = new String[10][3];
	
	/**
	 * Create the frame.
	 */
	public Adopciones() {
		getContentPane().setLayout(null);
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(Adopciones.class.getResource("/Documentos/Huella.png")));
		this.adopciones[0][0]="Codigo Mascota";
		this.adopciones[0][1]="Nombre Mascota";
		this.adopciones[0][2]="DNI Adoptante";
		for (int i=1;i<adopciones.length;i++){
			for(int j=0;j<3;j++){
				this.adopciones[i][j]="";
			}
			
		}
		
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(500, 200, 665, 417);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(Adopciones.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{adopciones[0][0],adopciones[0][1],adopciones[0][2]},
					{adopciones[1][0],adopciones[1][1],adopciones[1][2]},
					{adopciones[2][0],adopciones[2][1],adopciones[2][2]},
					{adopciones[3][0],adopciones[3][1],adopciones[3][2]},
					{adopciones[4][0],adopciones[4][1],adopciones[4][2]},
					{adopciones[5][0],adopciones[5][1],adopciones[5][2]},
					{adopciones[6][0],adopciones[6][1],adopciones[6][2]},
					{adopciones[7][0],adopciones[7][1],adopciones[7][2]},
					{adopciones[8][0],adopciones[8][1],adopciones[8][2]},
				},
				new String[] {
					"Codigo", "Nombre", "DNI Adoptante"
				}
			));
		table.setBounds(69, 164, 568, 144);
		contentPane.add(table);
		
		JButton btnNewButton = new JButton("A\u00F1adir/Editar");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton.setBounds(256, 320, 151, 20);
		contentPane.add(btnNewButton);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnActualizar.setBounds(256, 352, 151, 20);
		contentPane.add(btnActualizar);
		
		JLabel lblAdopciones = new JLabel("Adopciones");
		lblAdopciones.setFont(new Font("Dialog", Font.BOLD, 22));
		lblAdopciones.setBounds(262, 96, 145, 23);
		contentPane.add(lblAdopciones);
		

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(0, 0, 689, 485);
		lblNewLabel.setIcon(new ImageIcon(Adopciones.class.getResource("/Documentos/Fondo.jpg")));
		getContentPane().add(lblNewLabel);
		//EVENTOS
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarAdopciones wa = new ModificarAdopciones();
			}
		});
		
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					adopciones=db.getValues();
					añadirAdopcion(adopciones,table);
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
		//METODOS
		public void añadirAdopcion(String[][] array,JTable table){
			table.setModel(new DefaultTableModel(
					new Object[][] {
						{adopciones[0][0],adopciones[0][1],adopciones[0][2]},
						{adopciones[1][0],adopciones[1][1],adopciones[1][2]},
						{adopciones[2][0],adopciones[2][1],adopciones[2][2]},
						{adopciones[3][0],adopciones[3][1],adopciones[3][2]},
						{adopciones[4][0],adopciones[4][1],adopciones[4][2]},
						{adopciones[5][0],adopciones[5][1],adopciones[5][2]},
						{adopciones[6][0],adopciones[6][1],adopciones[6][2]},
						{adopciones[7][0],adopciones[7][1],adopciones[7][2]},
						{adopciones[8][0],adopciones[8][1],adopciones[8][2]},
					},
					new String[] {
						"Codigo", "Nombre", "DNI Adoptante"
					}
				));
		}
		public void elimminarAdopcion(String[][] array,JTable table){
				int a=5;
					for(int i=1;i<array.length;i++){
						for(int j=0;i<3;j++){
							if(array[i][j].equals("")){
								a=i;
							}
						}
					}
					adopciones[a][0]="";
					adopciones[a][1]="";
					adopciones[a][2]="";
			table.setModel(new DefaultTableModel(
					new Object[][] {
						{adopciones[0][0],adopciones[0][1],adopciones[0][2]},
						{adopciones[1][0],adopciones[1][1],adopciones[1][2]},
						{adopciones[2][0],adopciones[2][1],adopciones[2][2]},
						{adopciones[3][0],adopciones[3][1],adopciones[3][2]},
						{adopciones[4][0],adopciones[4][1],adopciones[4][2]},
						{adopciones[5][0],adopciones[5][1],adopciones[5][2]},
						{adopciones[6][0],adopciones[6][1],adopciones[6][2]},
						{adopciones[7][0],adopciones[7][1],adopciones[7][2]},
						{adopciones[8][0],adopciones[8][1],adopciones[8][2]},
					},
					new String[] {
						"Codigo", "Nombre", "DNI Adoptante"
					}
				));
		}
		public JTable getTable() {
			return table;
		}
		public void setTable(JTable table) {
			this.table = table;
		}
		public String[][] getAdopciones() {
			return adopciones;
		}
		public void setAdopciones(String[][] adopciones) {
			this.adopciones = adopciones;
	
}}
