package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SQL;

import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JEditorPane;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Font;

public class Menu extends JFrame {

	private JPanel contentPane;

	

	/**
	 * Create the frame.
	 */
	public Menu() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 200, 445, 468);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setForeground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		
		JButton btnInformes = new JButton("");
		btnInformes.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/info.png")));
		btnInformes.setBackground(Color.WHITE);
		btnInformes.setBounds(148, 85, 112, 112);
		contentPane.add(btnInformes);
		
		JButton btnEditarPerfil = new JButton("");
		btnEditarPerfil.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/crear_usu.png")));
		btnEditarPerfil.setBackground(Color.WHITE);
		btnEditarPerfil.setBounds(290, 85, 112, 112);
		contentPane.add(btnEditarPerfil);
		
		JButton btnVeterinario = new JButton("");
		btnVeterinario.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/vete.png")));
		btnVeterinario.setBackground(Color.WHITE);
		btnVeterinario.setBounds(290, 249, 112, 112);
		contentPane.add(btnVeterinario);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);
		
		JButton btnDesconectar = new JButton("Desconectar");
		btnDesconectar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDesconectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hide();
				Login wa= new Login();
			}
		});
		btnDesconectar.setBounds(272, 408, 130, 20);
		contentPane.add(btnDesconectar);
		
		JLabel label_5 = new JLabel("");
		label_5.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/crear_usu.png")));
		label_5.setBounds(468, 53, 99, 86);
		contentPane.add(label_5);
		
		JLabel label_7 = new JLabel("");
		label_7.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/vete.png")));
		label_7.setBounds(478, 225, 85, 74);
		contentPane.add(label_7);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/aniamales.png")));
		btnNewButton.setBounds(148, 249, 112, 112);
		contentPane.add(btnNewButton);
		
		JLabel lblInformar = new JLabel("Informar");
		lblInformar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblInformar.setBounds(176, 208, 84, 20);
		contentPane.add(lblInformar);
		
		JLabel lblCrearUsuario = new JLabel("Crear usuario");
		lblCrearUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCrearUsuario.setBounds(300, 208, 84, 20);
		contentPane.add(lblCrearUsuario);
		
		JLabel lblAnimales = new JLabel("Animales");
		lblAnimales.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAnimales.setBounds(176, 372, 84, 20);
		contentPane.add(lblAnimales);
		
		JLabel lblVeterinarios = new JLabel("Veterinarios");
		lblVeterinarios.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVeterinarios.setBounds(310, 372, 84, 20);
		contentPane.add(lblVeterinarios);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/Fondo.jpg")));
		label.setBounds(0, 0, 438, 439);
		contentPane.add(label);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				Animales wa = new Animales(/*"/Documentos/Logo.png"*/);
				//wa.rellenaFondo(getGraphics());
			}
		});
		btnVeterinario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				Veterinarios wa = new Veterinarios();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					wa.setVeterinario(db.getValues1());
					wa.aņadirVeterinarios(wa.getVeterinario(), wa.getTable());
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		});
		btnEditarPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				Crear_Usuario wa = new Crear_Usuario();
				wa.setVisible(true);
			}
		});
		btnInformes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				Informar wa = new Informar();
				wa.setVisible(true);
			}
		});
	}
}
