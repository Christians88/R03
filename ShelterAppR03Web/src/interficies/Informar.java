package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JTextPane;
import javax.swing.JTextArea;

public class Informar extends JFrame {

	private JPanel contentPane;
	private JTextField txtLogo;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Create the frame.
	 */
	public Informar() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Informar.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(500, 200, 536, 430);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		//Logo de la empresa
		JLabel lblLogo = new JLabel("LOGO");
		lblLogo.setIcon(new ImageIcon(Informar.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);

		JLabel lblInformar = new JLabel("Informar");
		lblInformar.setFont(new Font("Corbel", Font.BOLD, 22));
		lblInformar.setBounds(234, 58, 160, 23);
		contentPane.add(lblInformar);
		
		textField = new JTextField();
		textField.setBounds(328, 126, 170, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(328, 158, 170, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblPara = new JLabel("Para");
		lblPara.setBounds(234, 129, 46, 14);
		contentPane.add(lblPara);
		
		JLabel lblAsunto = new JLabel("Asunto");
		lblAsunto.setBounds(234, 161, 46, 14);
		contentPane.add(lblAsunto);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(234, 363, 89, 23);
		contentPane.add(btnEnviar);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(50, 191, 452, 160);
		contentPane.add(textPane);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Informar.class.getResource("/Documentos/Fondo.jpg")));
		label.setBounds(0, 0, 536, 408);
		contentPane.add(label);
	
	
	//EVENTOS
	
	btnEnviar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			enviarMail();
		}
	});
	}
	
	//METODOS
	
	public void enviarMail() {
		Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        final String Usuario ="shelterappr03@gmail.com";
        final String Contraseņa = "ShelterApproot";

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Usuario, Contraseņa);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Usuario));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(/*to*/textField.getText()));
            message.setSubject(/*subject*/textField_1.getText());
            message.setText(/*mensaje*/"");//NO ME VA EL GETTEXT DEL TEXTPANE,HAY QUE REVISAR PORQUE

            Transport.send(message);
            JOptionPane.showMessageDialog(this, "Su mensaje ha sido enviado");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
