package SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class SQL {
	
	private static Connection Conexion = null;

	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexion con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexion con el servidor");
	        }
	    }
         
	//METODO QUE FINALIZA CONEXION A SQL
	public void closeConnection() {
	        try {
	            Conexion.close();
	            
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
	        }
	    }
    
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
	 public void createTable(String name) {
	        try {
	            String Query = "CREATE TABLE " + name + ""
	                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
	                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	 
	 //METODO QUE ELIMINA TABLA EN NUESTRA BASE DE DATOS
	 public void deleteTable(String name) {
		try{
			String Query = "DROP TABLE " + name + " cascade constraints";
			Statement st = Conexion.createStatement();
	        st.executeUpdate(Query);
	        JOptionPane.showMessageDialog(null, "Se ha eliminado la tabla " + name + " de forma exitosa");
		} catch (SQLException ex){
			Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		}
	 }
	 		
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, int ID) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE DI = " + ID ;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla eliminada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecordMarta(String table_name, String CODIGO_MASCOTA) {
		        try {
		            String Query = "DELETE FROM " + table_name + " WHERE CODIGO_MASCOTA = " + CODIGO_MASCOTA ;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente");

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecordtesting(String DNI) {
		        try {
		        	 String Query = "DELETE FROM VETERINARIO" + " WHERE DNI = '" + DNI +"'" ;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente");

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }	 
		 
	//METODO QUE EDITA VALORES DEL STIPO STRING DE NUESTRA BASE DE DATOS
	 
	 public void editSTRINGRecord(String table_name, int ID, String campo, String nuevo_valor ){
		 try {   
			 String Query = "UPDATE " + table_name + " SET " + campo + " = " + "'" + nuevo_valor + "'" + " WHERE DI = " + ID;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla editada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	        }
	 }
	 
	//METODO QUE EDITA VALORES DEL STIPO INT DE NUESTRA BASE DE DATOS
	 
	 public void editINTRecord(String table_name, int ID, int campo, String nuevo_valor ){
		 try {
	            String Query = "UPDATE " + table_name + " SET " + campo + "=" + nuevo_valor + " where DI = " + ID;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla editada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	        }
	 }
	//METODO QUE EDITA VALORES DEL TIPO STRING DE NUESTRA BASE DE DATOS
	 
		 public void editSTRINGRecordMarta(String table_name, String CODIGO_MASCOTA, String campo, String nuevo_valor ){
			 try {
		            String Query = "UPDATE " + table_name + " SET " + campo + " = " + "'" + nuevo_valor + "'" + " where CODIGO_MASCOTA = " + CODIGO_MASCOTA;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            //JOptionPane.showMessageDialog(null, "Tupla editada");
		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		 }
		 
		//METODO QUE EDITA VALORES DEL TIPO STRING DE NUESTRA BASE DE DATOS
		 
		 public void editSTRINGtesting(String table_name, String DNI_VET, String campo, String nuevo_valor ){
			 try {
		            String Query = "UPDATE " + table_name + " SET " + campo + " = " + "'" + nuevo_valor + "'" + " where DNI = '" + DNI_VET+"'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            //JOptionPane.showMessageDialog(null, "Tupla editada");
		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		 }
		 
		 
		//METODO QUE EDITA VALORES DEL TIPO INT DE NUESTRA BASE DE DATOS
		 
		 public void editINTRecordMarta(String table_name, String CODIGO_MASCOTA, String campo, String nuevo_valor ){
			 try {
		            String Query = "UPDATE " + table_name + " SET " + campo + "=" + nuevo_valor + " where CODIGO_MASCOTA = " + CODIGO_MASCOTA;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            //JOptionPane.showMessageDialog(null, "Tupla editada");

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		 }
	 //METODOS PERSONALIZADOS PARA CADA TABLA
	 
	 // ------------------TABLA ADOPCIONES-----------------------------------------
	 
	//METODO QUE INSERTA TUPLAS
		 public void insertData(String table_name, String codigo, String codigoPerrera, String dniVeterinario, String tipoMascota,
				 String nombre, String raza, String tama�o, char sexo, String adopcion, String DNIDue�o, String salud, 
				 String color, String foto) {
		        try {
		            String Query = "INSERT INTO " + table_name + "(di,codigo,codigo_perrera,dni_veterinario,tipo_mascota,nombre,raza,tama�o,sexo,adopcion,dni_due�o,color,foto)"
		            		+ " VALUES("
		            		+ "'"+ codigo + "',"
		            		+ "'"+ codigoPerrera + "',"
		            		+ "'"+ dniVeterinario + "',"
		            		+ "'"+ tipoMascota + "',"
		            		+ "'"+ nombre + "'," 
		            		+ "'"+ raza + "',"
		            		+ "'"+ tama�o + "',"
		            		+ "'"+ sexo + "',"
		            		+ "'"+ adopcion + "',"
		            		+ "'"+ DNIDue�o + "',"
		            		+ "'"+ salud + "',"
		            		+ "'"+ color + "',"
		            		+ "'"+ foto + "'," + "')";
		            		   		
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		 
	//METODO QUE EDITA TUPLAS
		 public void editAdopciones(int ID, String nuevo_valor ){
			 try {   
				 String Query = "UPDATE " + "MASCOTAS" + " SET " + "DNI_DUE�O" + " = " + "'" + nuevo_valor + "'" + " WHERE DI = " + ID;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Tupla editada");

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		 }
		 
	//METODO QUE ELIMINA TUPLAS
		 public void eliminaAdopcion(int ID){
			 try {   
				 String Query = "UPDATE " + "MASCOTAS" + " SET DNI_DUE�O = NULL WHERE DI = " + ID;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Adopcion eliminada");
		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		 }
		 
		//METODO CONSULTAS SQL
			public String[][] getValues() {
				
				
				String array[][]=new String[10][3];
				array[0][0]="Codigo Mascota";
				array[0][1]="Nombre Mascota";
				array[0][2]="DNI Adoptante";
				for (int i=1;i<array.length;i++){
					for(int j=0;j<3;j++){
						array[i][j]="";
					}
				}
				int i=1;
			   try {
			            String Query = "SELECT CODIGO,NOMBRE,DNI_DUE�O FROM MASCOTAS";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			           while (resultSet.next()) {
			                        array[i][0]= resultSet.getString("CODIGO");
			                        array[i][1]= resultSet.getString("NOMBRE");
			                        array[i][2]= resultSet.getString("DNI_DUE�O");
			                        i++;
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			        }
			   	return(array);
			    }
			
	//---------------------------TABLA Veterinarios-----------------------------------------
	// METODO QUE INSERTA TUPLAS
	public void insertData1(String DNI, String CIF_PROTECTORA, String NOMBRE, String APELLIDO1, String APELLIDO2) {
       try {
           String Query = "INSERT INTO VETERINARIO" + 
           		" VALUES("
           		+ "di_seq3.NEXTVAL,"
           		+ "'"+ DNI + "',"
           		+ "'"+ CIF_PROTECTORA + "',"
           		+ "'"+ NOMBRE + "',"
           		+ "'"+ APELLIDO1 + "',"
           		+ "'"+ APELLIDO2 + "')";
           		
           Statement st = Conexion.createStatement();
           st.executeUpdate(Query);
           JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
       }
	}
	//METODO QUE HACE CONSULTAS SQL		
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
			public String[][]getValues1() {
				
				String array[][]=new String[10][5];
				array[0][0]="DNI";
				array[0][1]="CIF_PERRERA";
				array[0][2]="NOMBRE";
				array[0][3]="APELLIDO1";
				array[0][4]="APELLIDO2";
				for (int i=1;i<array.length;i++){
					for(int j=1;j<5;j++){
						array[i][j]="";
					}
					
				}
				int i=1;
			   try {
			            String Query = "SELECT DNI,CIF_PERRERA,NOMBRE,APELLIDO1,APELLIDO2 FROM VETERINARIO";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			           while (resultSet.next()) {
			                        array[i][0]= resultSet.getString("DNI");
			                        array[i][1]= resultSet.getString("CIF_PERRERA");
			                        array[i][2]= resultSet.getString("NOMBRE");
			                        array[i][3]= resultSet.getString("APELLIDO1");
			                        array[i][4]= resultSet.getString("APELLIDO2");
			                        i++;
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			        }
			   	return(array);
			    }
	//--------------------------TABLA MASCOTAS---------------------------------------
	//METODO HACE CONSULTAS SQL
	public String[][] getValues2() {
		String array[][]=new String[9][12];
		array[0][0]="CODIGO";
		array[0][1]="CODIGO_PERRERA";
		array[0][2]="DNI_VETERINARIO";
		array[0][3]="TIPO_MASCOTA";
		array[0][4]="NOMBRE";
		array[0][5]="RAZA";
		array[0][6]="TAMA�O";
		array[0][7]="SEXO";
		array[0][8]="DNI_DUE�O";
		array[0][9]="SALUD";
		array[0][10]="COLOR";
		array[0][11]="FOTO";
		for (int i=1;i<array.length;i++){
			for(int j=0;j<11;j++){
				array[i][j]="";
			}
		}
		int i=1;
	   try {
	            String Query = "SELECT CODIGO,CODIGO_PERRERA,DNI_VETERINARIO,TIPO_MASCOTA,NOMBRE,RAZA,TAMA�O,SEXO,DNI_DUE�O,SALUD,COLOR,FOTO FROM MASCOTAS";
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);
	           while (resultSet.next()) {
	                        array[i][0]= resultSet.getString("CODIGO");
	                        array[i][1]= resultSet.getString("CODIGO_PERRERA");
	                        array[i][2]= resultSet.getString("DNI_VETERINARIO");
	                        array[i][3]= resultSet.getString("TIPO_MASCOTA");
	                        array[i][4]= resultSet.getString("NOMBRE");
	                        array[i][5]=resultSet.getString("RAZA");
	                		array[i][6]=resultSet.getString("TAMA�O");
	                		array[i][7]=resultSet.getString("SEXO");
	                		array[i][8]=resultSet.getString("DNI_DUE�O");
	                		array[i][9]=resultSet.getString("SALUD");
	                		array[i][10]=resultSet.getString("COLOR");
	                		array[i][11]=resultSet.getString("FOTO");
	                        i++;
	            }
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	        }
	   	return(array);
	}
	
	//METODO QUE INSERTA TUPLAS
		public void insertData2(String CODIGO, String CODIGO_PERRERA, String DNI_VETERINARIO, String TIPO_MASCOTA, String NOMBRE,
				String RAZA, String TAMA�O, String SEXO, String DNI_DUE�O, String SALUD, String COLOR, String FOTO) {
		       try {
		           String Query = "INSERT INTO " + "MASCOTAS" + 
		           		" VALUES("
		           		+ "di_seq4.NEXTVAL,"
		           		+ "'"+ CODIGO + "',"
		           		+ "'"+ CODIGO_PERRERA + "',"
		           		+ "'"+ DNI_VETERINARIO + "',"
		           		+ "'"+ TIPO_MASCOTA + "',"
		           		+ "'"+ NOMBRE + "',"
		           		+ "'"+ RAZA + "',"
		           		+ "'"+ TAMA�O + "',"
		           		+ "'"+ SEXO + "',"
		           		+ "'"+ DNI_DUE�O + "',"
		           		+ "'"+ SALUD + "',"
		           		+ "'"+ COLOR + "',"
		           		+ "'"+ FOTO + "')";
		           		
		           Statement st = Conexion.createStatement();
		           st.executeUpdate(Query);
		           JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		       } catch (SQLException ex) {
		           JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		       }
			}
	//--------------------------TABLA ACOGIDA-----------------------------------------
	//METODO HACE CONSULTAS SQL
		public String[][] getValues3() {
			String array[][]=new String[10][12];
			array[0][0]="Codigo Mascota";
			array[0][1]="DNI_Adoptante";
			array[0][2]="Tiempo";
			for (int i=1;i<array.length;i++){
				for(int j=0;j<3;j++){
					array[i][j]="";
				}
			}
			int i=1;
		   try {
		            String Query = "SELECT CODIGO_MASCOTA,DNI, TIEMPO FROM ACOGIDA";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		           while (resultSet.next()) {
		                        array[i][0]= resultSet.getString("CODIGO_MASCOTA");
		                        array[i][1]= resultSet.getString("DNI");
		                        array[i][2]= resultSet.getString("TIEMPO");
		                        i++;
		            }
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
		        }
		   	return(array);
		}
		// METODO QUE INSERTA TUPLAS
		public void insertData3( String CODIGO_MASCOTA, String DNI, String TIEMPO) {
	       try {
	           String Query = "INSERT INTO " + "ACOGIDA" + 
	           		" VALUES("
	           		+ "di_seq7.NEXTVAL,"
	           		+ "'"+ CODIGO_MASCOTA + "',"
	           		+ "'"+ DNI + "',"
	           		+ "'"+ TIEMPO +  "')";
	           Statement st = Conexion.createStatement();
	           st.executeUpdate(Query);
	           JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	       } catch (SQLException ex) {
	           JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	       }
		}
	
	//--------------------------LOGGIN----------------------------------------
	//METODO CONSULTA SQL USUARIOS
		public boolean log(String user, String pass) {
			boolean condition = false;
		   try {
		            String Query = "SELECT CIF,PASS FROM PERRERA";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		           while (resultSet.next()) {   
		        	   if ((user.equals(resultSet.getString("CIF")))&&(pass.equals(resultSet.getString("PASS")))){
		                    condition = true;
		               }
		                        
		            }
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
		        }
		   	return(condition);	
		}
		
		// METODO QUE INSERTA TUPLAS
				public void insertData4( String CIF, String NOMBRE, String DIRECCION,int TELEFONO, String EMAIL, 
						String LOCALIDAD, String PROVINCIA, String NUMERO_CUENTA, char[] PASS) {
			       try {
			           String Query = "INSERT INTO " + "PERRERA" + 
			           		" VALUES("
			           		+ "di_seq1.NEXTVAL,"
			           		+ "'"+ CIF + "',"
			           		+ "'"+ NOMBRE + "',"
			           		+ "'"+ DIRECCION + "',"
			           		+ "'"+ TELEFONO + "',"
			           		+ "'"+ EMAIL + "',"
			           		+ "'"+ LOCALIDAD + "',"
			           		+ "'"+ PROVINCIA+ "',"
			           		+ "'"+ NUMERO_CUENTA +  "',"
			           		+ "'"+ PASS + "')";
			           Statement st = Conexion.createStatement();
			           st.executeUpdate(Query);
			           JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			       } catch (SQLException ex) {
			           JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			       }
				}
			
	//-----------------------------TABLA APADRINAMIENTOS------------------------------
	//METODO QUE INSERTA TUPLAS
		 public void insertData8(String table_name, String dni,String Codigo_mascota, String Importe) {
		        try {
		        	int maxID = 0;
		        	Statement s2 = Conexion.createStatement();
		        	s2.execute("SELECT MAX(DI) FROM " + table_name );    
		        	ResultSet rs2 = s2.getResultSet(); // 
		        	if ( rs2.next() ){
		        	  maxID = rs2.getInt(1);
		        	}
		        	s2.close();
		        	maxID = maxID + 1;
		        	
		            String Query = "INSERT INTO " + table_name + " (di, dni, codigo_mascota, importe)"
		            		+ " VALUES ("
		            		+ maxID +","
		            		+ "'"+ dni + "',"
		            		+ "'"+ Codigo_mascota + "',"
		            		+ ""+ Importe + ")";
		            
		            Statement st = Conexion.createStatement();
		            //st.executeUpdate(Query);
		            st.execute(Query);
		            //st.executeQuery(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
	
		//METODO QUE HACE CONSULTAS SQL			
			public String[][]getValues8() {
				
				String array[][]=new String[10][3];
				array[0][0]="DNI PADRINO";
				array[0][1]="CODIGO ANIMAL";
				array[0][2]="IMPORTE";
				for (int i=1;i<array.length;i++){
					for(int j=0;j<3;j++){
						array[i][j]="";
					}	
				}
				int i=1;
			   try {
			            String Query = "SELECT DNI, CODIGO_MASCOTA, IMPORTE FROM APADRINA";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			           while (resultSet.next()) {
			                        array[i][0]= resultSet.getString("DNI");
			                        array[i][1]= resultSet.getString("CODIGO_MASCOTA");
			                        array[i][2]= resultSet.getString("IMPORTE");
			                        i++;
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			        }
			   	return(array);
			    }
			
			//COMBOBOX
			public String[]getValuesCombo() {
				
				String array[]=new String[10];

			   try {
				   int i = 0;
			            String Query = "SELECT CODIGO FROM MASCOTAS";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			           while (resultSet.next()) {
			                        array[i]= resultSet.getString("CODIGO");
			                        i++;
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			        }
			   	return(array);
			    }
			
	//METODO QUE ELIMINA TUPLAS
			 public void deleteRecord8(String table_name, String CODIGO_MASCOTA) {
			        try {
			            String Query = "DELETE FROM " + table_name + " WHERE CODIGO_MASCOTA = " + CODIGO_MASCOTA ;
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente");

			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
			        }
			    }
}



