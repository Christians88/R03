package WIPMarta;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WIPMarta.SQL;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;


public class AņadirApadrinamientos extends JFrame {
	
	private JPanel contentPane;
	private JTextField textField_DNI;
	private JTextField textField_Imp;
	public JComboBox Codigo = new JComboBox();
	public String codigo_mascota;
	public String mascotas[];
	

	/**
	 * Launch the application.
	 */
	
	

	/**
	 * Create the frame.
	 */
	public AņadirApadrinamientos() {
		
	
		setTitle("ShelterApp");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\root\\Desktop\\icono.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 689, 485);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setVisible(true);
		
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(395, 367, 169, 29);
		contentPane.add(btnAceptar);
		
		JLabel lblCdigoDelAnimal = new JLabel("C\u00F3digo del animal");
		lblCdigoDelAnimal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCdigoDelAnimal.setBounds(173, 98, 134, 22);
		contentPane.add(lblCdigoDelAnimal);
		
		JLabel lblDniPadrino = new JLabel("DNI Padrino");
		lblDniPadrino.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDniPadrino.setBounds(173, 146, 110, 22);
		contentPane.add(lblDniPadrino);
		
		
		textField_DNI = new JTextField();
		textField_DNI.setColumns(10);
		textField_DNI.setBounds(307, 148, 120, 23);
		contentPane.add(textField_DNI);
		
		

		textField_Imp = new JTextField();
		textField_Imp.setColumns(10);
		textField_Imp.setBounds(307, 279, 120, 23);
		contentPane.add(textField_Imp);
	
		
		
		JLabel label = new JLabel("Importe");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(199, 277, 71, 22);
		contentPane.add(label);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(ModificarApadrinamientos.class.getResource("/Documentos/Logo.png")));
		lblNewLabel.setBounds(32, 21, 80, 84);
		contentPane.add(lblNewLabel);
		
		//Combo-box
		SQL db = new SQL();
		Codigo.setBounds(307, 101, 120, 20);
		 try {
			db.SQLConnection("ShelterApp", "root", "");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mascotas = db.getValuesCombo();
		db.closeConnection();
		for (int i = 0;i<10;i++){
			Codigo.addItem(mascotas[i]);
		}
		contentPane.add(Codigo);
		
		//Evento Aceptar
		
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 SQL db = new SQL();
				 int ind = Codigo.getSelectedIndex();
				 codigo_mascota = mascotas[ind];
				 try {
					db.SQLConnection("ShelterApp", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				 db.insertData8("APADRINA",textField_DNI.getText(),codigo_mascota,textField_Imp.getText());
				 db.closeConnection();
			     setVisible(false);
			     
			     
			}
		});
		


	}	
}
