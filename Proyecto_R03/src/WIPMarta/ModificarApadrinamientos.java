package WIPMarta;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WIPMarta.SQL;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.*;
import WIPMarta.ModificarApadrinamientos;
import WIPMarta.SQL;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;

public class ModificarApadrinamientos extends JFrame {

	private JPanel contentPane;
	private JTextField textField_CA;
	private JTextField textField_DNI;
	private JTextField textField_Imp;

	

	/**
	 * Launch the application.
	 */
	
	

	/**
	 * Create the frame.
	 */
	public ModificarApadrinamientos(String DNI, String CODIGO, String IMPORTE) {
		setTitle("ShelterApp");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\root\\Desktop\\icono.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 689, 485);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setVisible(true);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(395, 367, 169, 29);
		contentPane.add(btnAceptar);
		
		JLabel lblCdigoDelAnimal = new JLabel("C\u00F3digo del animal");
		lblCdigoDelAnimal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCdigoDelAnimal.setBounds(173, 98, 134, 22);
		contentPane.add(lblCdigoDelAnimal);
		
		JLabel lblDniPadrino = new JLabel("DNI Padrino");
		lblDniPadrino.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDniPadrino.setBounds(173, 146, 110, 22);
		contentPane.add(lblDniPadrino);
		
		textField_CA = new JTextField();
		textField_CA.setBounds(307, 98, 120, 23);
		textField_CA.setEditable(false);
		contentPane.add(textField_CA);
		textField_CA.setColumns(10);
		textField_CA.setText(CODIGO);
		
		textField_DNI = new JTextField();
		textField_DNI.setColumns(10);
		textField_DNI.setBounds(307, 148, 120, 23);
		contentPane.add(textField_DNI);
		textField_DNI.setText(DNI);
		

		textField_Imp = new JTextField();
		textField_Imp.setColumns(10);
		textField_Imp.setBounds(307, 279, 120, 23);
		contentPane.add(textField_Imp);
		textField_Imp.setText(IMPORTE);
		
		
		JLabel label = new JLabel("Importe");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(199, 277, 71, 22);
		contentPane.add(label);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(ModificarApadrinamientos.class.getResource("/Documentos/Logo.png")));
		lblNewLabel.setBounds(32, 21, 80, 84);
		contentPane.add(lblNewLabel);
		
		
		//Eventos
		//Aceptar
		
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 SQL db = new SQL();
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.editINTRecord("APADRINA", textField_CA.getText(), "IMPORTE", textField_Imp.getText());
					db.editSTRINGRecord("APADRINA", textField_CA.getText(), "DNI", textField_DNI.getText());
					db.closeConnection();
					setVisible(false);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
	}
}
				
				

