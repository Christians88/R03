package WIPMarta;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class SQL {
	private static Connection Conexion = null;

	//METODO PERSONALIZADO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexion con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexion con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexion con el servidor");
	        }
	    }
         
	//METODO QUE FINALIZA LA CONEXION A SQL
	public void closeConnection() {
	        try {
	            Conexion.close();
	            //JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
	        } catch (SQLException ex) {
	           
	            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
	        }
	    }
    
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
	 public void createTable(String name) {
	        try {
	            String Query = "CREATE TABLE " + name + ""
	                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
	                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	 
	 //METODO QUE ELIMINA TABLA EN NUESTRA BASE DE DATOS
	 public void deleteTable(String name) {
		try{
			String Query = "DROP TABLE " + name + " cascade constraints";
			Statement st = Conexion.createStatement();
	        st.executeUpdate(Query);
	        JOptionPane.showMessageDialog(null, "Se ha eliminado la tabla " + name + " de forma exitosa");
		} catch (SQLException ex){
			Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		}
	 }
	 		
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, String CODIGO_MASCOTA) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE CODIGO_MASCOTA = " + CODIGO_MASCOTA ;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }
	
	//METODO QUE EDITA VALORES DEL TIPO STRING DE NUESTRA BASE DE DATOS
	 
	 public void editSTRINGRecord(String table_name, String CODIGO_MASCOTA, String campo, String nuevo_valor ){
		 try {
	            String Query = "UPDATE " + table_name + " SET " + campo + " = " + "'" + nuevo_valor + "'" + " where CODIGO_MASCOTA = " + CODIGO_MASCOTA;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            //JOptionPane.showMessageDialog(null, "Tupla editada");
	            

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	        }
	 }
	 
	//METODO QUE EDITA VALORES DEL TIPO INT DE NUESTRA BASE DE DATOS
	 
	 public void editINTRecord(String table_name, String CODIGO_MASCOTA, String campo, String nuevo_valor ){
		 try {
	            String Query = "UPDATE " + table_name + " SET " + campo + "=" + nuevo_valor + " where CODIGO_MASCOTA = " + CODIGO_MASCOTA;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            //JOptionPane.showMessageDialog(null, "Tupla editada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	        }
	 }
	 
	 //METODOS PERSONALIZADOS PARA CADA TABLA
	 
	 // TABLA APRADINAMIENTOS-----------------------------------------
	 
	//METODO QUE INSERTA TUPLAS
	 public void insertData8(String table_name, String dni,String Codigo_mascota, String Importe) {
	        try {
	        	int maxID = 0;
	        	Statement s2 = Conexion.createStatement();
	        	s2.execute("SELECT MAX(DI) FROM " + table_name );    
	        	ResultSet rs2 = s2.getResultSet(); // 
	        	if ( rs2.next() ){
	        	  maxID = rs2.getInt(1);
	        	}
	        	s2.close();
	        	maxID = maxID + 1;
	        	
	            String Query = "INSERT INTO " + table_name + " (di, dni, codigo_mascota, importe)"
	            		+ " VALUES ("
	            		+ maxID +","
	            		+ "'"+ dni + "',"
	            		+ "'"+ Codigo_mascota + "',"
	            		+ ""+ Importe + ")";
	            
	            Statement st = Conexion.createStatement();
	            //st.executeUpdate(Query);
	            st.execute(Query);
	            //st.executeQuery(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	    }	

	//METODO QUE HACE CONSULTAS SQL			
		public String[][]getValues8() {
			
			String array[][]=new String[10][3];
			array[0][0]="DNI PADRINO";
			array[0][1]="CODIGO ANIMAL";
			array[0][2]="IMPORTE";
			for (int i=1;i<array.length;i++){
				for(int j=0;j<3;j++){
					array[i][j]="";
				}	
			}
			int i=1;
		   try {
		            String Query = "SELECT DNI, CODIGO_MASCOTA, IMPORTE FROM APADRINA";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		           while (resultSet.next()) {
		                        array[i][0]= resultSet.getString("DNI");
		                        array[i][1]= resultSet.getString("CODIGO_MASCOTA");
		                        array[i][2]= resultSet.getString("IMPORTE");
		                        i++;
		            }
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
		        }
		   	return(array);
		    }
		
		//COMBOBOX
		public String[]getValuesCombo() {
			
			String array[]=new String[10];

		   try {
			   int i = 0;
		            String Query = "SELECT CODIGO FROM MASCOTAS";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		           while (resultSet.next()) {
		                        array[i]= resultSet.getString("CODIGO");
		                        i++;
		            }
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
		        }
		   	return(array);
		    }
		
//METODO QUE ELIMINA TUPLAS
		 public void deleteRecord8(String table_name, String CODIGO_MASCOTA) {
		        try {
		            String Query = "DELETE FROM " + table_name + " WHERE CODIGO_MASCOTA = " + CODIGO_MASCOTA ;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Registro eliminado correctamente");

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
}



