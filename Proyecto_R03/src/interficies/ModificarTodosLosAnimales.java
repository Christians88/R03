package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SQL;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Font;

public class ModificarTodosLosAnimales extends JFrame {

	private JPanel contentPane;
	private JTextField Codigo;
	private JTextField Nombre;
	private JTextField Raza;
	private JTextField Salud;
	private JTextField Color;
	private JTextField Cod_Perrera;
	private JTextField DNI_Du;
	private JTextField DNI_Vet;
	private JTextField Foto;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField ID;

	/**
	 * Create the frame.
	 */
	
	public ModificarTodosLosAnimales() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ModificarTodosLosAnimales.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 498, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		
		Codigo = new JTextField();
		Codigo.setBounds(309, 44, 170, 20);
		contentPane.add(Codigo);
		Codigo.setColumns(10);
		
		Nombre = new JTextField();
		Nombre.setBounds(309, 75, 170, 20);
		contentPane.add(Nombre);
		Nombre.setColumns(10);
		
		Raza = new JTextField();
		Raza.setBounds(309, 106, 170, 20);
		contentPane.add(Raza);
		Raza.setColumns(10);
		
		Salud = new JTextField();
		Salud.setBounds(309, 137, 170, 20);
		contentPane.add(Salud);
		Salud.setColumns(10);
		
		Color = new JTextField();
		Color.setBounds(309, 168, 170, 20);
		contentPane.add(Color);
		Color.setColumns(10);
		
		Cod_Perrera = new JTextField();
		Cod_Perrera.setBounds(309, 198, 170, 20);
		contentPane.add(Cod_Perrera);
		Cod_Perrera.setColumns(10);
		
		DNI_Du = new JTextField();
		DNI_Du.setBounds(309, 229, 170, 20);
		contentPane.add(DNI_Du);
		DNI_Du.setColumns(10);
		
		DNI_Vet = new JTextField();
		DNI_Vet.setBounds(309, 260, 170, 20);
		contentPane.add(DNI_Vet);
		DNI_Vet.setColumns(10);
		
		final JComboBox Tipo = new JComboBox();
		Tipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		Tipo.setBounds(309, 291, 170, 20);
		contentPane.add(Tipo);
		Tipo.addItem("PERRO");
		Tipo.addItem("GATO");
		
		final JRadioButton rdbtnNewRadioButton = new JRadioButton("M");
		rdbtnNewRadioButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(309, 321, 45, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("H");
		rdbtnNewRadioButton_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(381, 321, 48, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		final JComboBox Tama�o = new JComboBox();
		Tama�o.setFont(new Font("Tahoma", Font.PLAIN, 14));
		Tama�o.setBounds(309, 353, 170, 20);
		contentPane.add(Tama�o);
		Tama�o.addItem("PEQUE�O");
		Tama�o.addItem("MEDIANO");
		Tama�o.addItem("GRANDE");
		
		Foto = new JTextField();
		Foto.setBounds(309, 384, 170, 20);
		contentPane.add(Foto);
		Foto.setColumns(10);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipo.setBounds(165, 291, 46, 20);
		contentPane.add(lblTipo);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSexo.setBounds(165, 321, 46, 20);
		contentPane.add(lblSexo);
		
		JLabel lblTamao = new JLabel("Tama\u00F1o");
		lblTamao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTamao.setBounds(165, 353, 146, 20);
		contentPane.add(lblTamao);
		
		JLabel lblFoto = new JLabel("Foto");
		lblFoto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFoto.setBounds(165, 384, 46, 20);
		contentPane.add(lblFoto);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCodigo.setBounds(165, 43, 46, 20);
		contentPane.add(lblCodigo);
		
		JLabel lblCodigoPerrera = new JLabel("Codigo Perrera");
		lblCodigoPerrera.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCodigoPerrera.setBounds(165, 198, 146, 20);
		contentPane.add(lblCodigoPerrera);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(165, 74, 101, 20);
		contentPane.add(lblNombre);
		
		JLabel lblRaza = new JLabel("Raza");
		lblRaza.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblRaza.setBounds(165, 105, 46, 20);
		contentPane.add(lblRaza);
		
		JLabel lblSalud = new JLabel("Salud");
		lblSalud.setBounds(165, 136, 46, 20);
		contentPane.add(lblSalud);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblColor.setBounds(165, 167, 46, 20);
		contentPane.add(lblColor);
		
		JLabel lblDniDueo = new JLabel("DNI Due\u00F1o");
		lblDniDueo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDniDueo.setBounds(165, 229, 117, 20);
		contentPane.add(lblDniDueo);
		
		JLabel lblDniVeterinario = new JLabel("DNI Veterinario");
		lblDniVeterinario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDniVeterinario.setBounds(165, 260, 146, 20);
		contentPane.add(lblDniVeterinario);
		
		JButton btnAadireditar = new JButton("A\u00F1adir/Editar");
		btnAadireditar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAadireditar.setBounds(165, 446, 113, 20);
		contentPane.add(btnAadireditar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnEliminar.setBounds(165, 477, 113, 20);
		contentPane.add(btnEliminar);
		
		ID = new JTextField();
		ID.setBounds(309, 415, 170, 20);
		contentPane.add(ID);
		ID.setColumns(10);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(165, 415, 33, 14);
		contentPane.add(lblId);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(ModificarTodosLosAnimales.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ModificarTodosLosAnimales.class.getResource("/Documentos/Fondo.jpg")));
		label.setBounds(0, 0, 492, 501);
		contentPane.add(label);
	
	//EVENTOS
	//A�ADIR & EDITAR
	btnAadireditar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String sexo="";
			if(rdbtnNewRadioButton.isSelected()){
				sexo="M";
			}
			else{
				sexo="H";
			}
			
			 SQL db = new SQL();
			 try {
				db.SQLConnection("ShelterApp", "root", "");
				db.insertData2(Codigo.getText(),Cod_Perrera.getText(),DNI_Vet.getText(),Tipo.getSelectedItem().toString(),Nombre.getText(),
						Raza.getText(),Tama�o.getSelectedItem().toString(),sexo,DNI_Du.getText(),Salud.getText(),Color.getText(),Foto.getText());
				db.closeConnection();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
			
		}
	});
	btnEliminar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			 SQL db = new SQL();
			 try {
				db.SQLConnection("ShelterApp", "root", "");
				db.deleteRecord("MASCOTAS", Integer.parseInt(ID.getText()) );
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	});
	
	}
}