package interficies;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import SQL.SQL;

import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.Font;

public class ModificarAcogida extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	/**
	 * Create the frame.
	 */
	public ModificarAcogida() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ModificarAcogida.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 509, 227);
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		
		
		JLabel lblLogo = new JLabel("LOGO");
		lblLogo.setIcon(new ImageIcon(ModificarAcogida.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);
		
		textField = new JTextField();
		textField.setBounds(309, 106, 170, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnAadir = new JButton("A\u00F1adir/Editar");
		btnAadir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAadir.setBackground(Color.WHITE);
		btnAadir.setBounds(165, 166, 113, 20);
		contentPane.add(btnAadir);
		
		JLabel lblTiempoAcogida = new JLabel("Tiempo Acogida");
		lblTiempoAcogida.setBounds(165, 135, 131, 20);
		contentPane.add(lblTiempoAcogida);
		
		JLabel lblTiempoAcogida_1 = new JLabel("ID Acogedor");
		lblTiempoAcogida_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTiempoAcogida_1.setBounds(165, 104, 117, 20);
		contentPane.add(lblTiempoAcogida_1);
		
		JLabel lblId = new JLabel("C\u00F3digo Animal");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblId.setBounds(165, 73, 117, 20);
		contentPane.add(lblId);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(309, 75, 170, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(309, 137, 170, 20);
		contentPane.add(textField_2);
		
		JLabel lblIdAnimal = new JLabel("ID Animal");
		lblIdAnimal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIdAnimal.setBounds(165, 43, 92, 20);
		contentPane.add(lblIdAnimal);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(309, 44, 170, 20);
		contentPane.add(textField_3);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(ModificarAcogida.class.getResource("/Documentos/Fondo.jpg")));
		lblNewLabel.setBounds(0, 0, 507, 205);
		contentPane.add(lblNewLabel);
		

		//EVENTOS
		//A�ADIR/EDITAR
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 SQL db = new SQL();
				 try {
					 db.SQLConnection("ShelterApp", "root", "");
					 db.insertData3(textField_1.getText(), textField.getText(),textField_2.getText());
					 db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
			}
		});
	}
}
