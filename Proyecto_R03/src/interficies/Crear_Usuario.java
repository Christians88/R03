package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import SQL.SQL;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JEditorPane;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.Color;

public class Crear_Usuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtCIF;
	private JTextField txtNOMBRE;
	private JTextField txtDIRECCION;
	private JTextField txtLOCALIDAD;
	private JTextField txtEMAIL;
	private JTextField txtTELEFONO;
	private JTextField txtPROVINCIA;
	private JTextField txtCUENTABANCARIA;
	private JLabel lblNewLabel;
	private JPasswordField passwordField;
	private JLabel label;
	private JPasswordField passwordField_1;

	/**
	 * Create the frame.
	 */
	public Crear_Usuario() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Crear_Usuario.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 514, 422);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ModificarVeterinarios.class.getResource("/Documentos/Logo.png")));
		label.setBounds(50, 11, 81, 108);
		contentPane.add(label);
		
		txtCIF = new JTextField();
		txtCIF.setBounds(309, 42, 170, 20);
		contentPane.add(txtCIF);
		txtCIF.setColumns(10);
		
		txtNOMBRE = new JTextField();
		txtNOMBRE.setBounds(309, 73, 170, 20);
		contentPane.add(txtNOMBRE);
		txtNOMBRE.setColumns(10);
		
		txtDIRECCION = new JTextField();
		txtDIRECCION.setBounds(309, 104, 170, 20);
		contentPane.add(txtDIRECCION);
		txtDIRECCION.setColumns(10);
		
		txtLOCALIDAD = new JTextField();
		txtLOCALIDAD.setBounds(309, 197, 170, 20);
		contentPane.add(txtLOCALIDAD);
		txtLOCALIDAD.setColumns(10);
		
		txtEMAIL = new JTextField();
		txtEMAIL.setBounds(309, 166, 170, 20);
		contentPane.add(txtEMAIL);
		txtEMAIL.setColumns(10);
		
		txtTELEFONO = new JTextField();
		txtTELEFONO.setBounds(309, 135, 170, 20);
		contentPane.add(txtTELEFONO);
		txtTELEFONO.setColumns(10);
		
		txtPROVINCIA = new JTextField();
		txtPROVINCIA.setBounds(309, 228, 170, 20);
		contentPane.add(txtPROVINCIA);
		txtPROVINCIA.setColumns(10);
		
		txtCUENTABANCARIA = new JTextField();
		txtCUENTABANCARIA.setBounds(309, 259, 170, 20);
		contentPane.add(txtCUENTABANCARIA);
		txtCUENTABANCARIA.setColumns(10);
		
		JLabel lblNombre = new JLabel("CIF");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(165, 42, 50, 20);
		contentPane.add(lblNombre);
		
		JLabel lblPrimerApellido = new JLabel("Nombre");
		lblPrimerApellido.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPrimerApellido.setBounds(165, 73, 86, 20);
		contentPane.add(lblPrimerApellido);
		
		JLabel lblSegundoApelllido = new JLabel("Direcci�n");
		lblSegundoApelllido.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSegundoApelllido.setBounds(165, 104, 91, 20);
		contentPane.add(lblSegundoApelllido);
		
		JLabel lblDni = new JLabel("Tel�fono");
		lblDni.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDni.setBounds(165, 135, 62, 20);
		contentPane.add(lblDni);
		
		JLabel lblNombreAnimales = new JLabel("Email");
		lblNombreAnimales.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombreAnimales.setBounds(165, 166, 91, 20);
		contentPane.add(lblNombreAnimales);
		
		JLabel lblTipoDeAnimal = new JLabel("Localidad");
		lblTipoDeAnimal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipoDeAnimal.setBounds(165, 197, 91, 20);
		contentPane.add(lblTipoDeAnimal);
		
		JLabel lblLocalidad = new JLabel("Prov�ncia");
		lblLocalidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLocalidad.setBounds(165, 228, 91, 20);
		contentPane.add(lblLocalidad);
		
		JLabel lblProvincia = new JLabel("Cuenta bancaria");
		lblProvincia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProvincia.setBounds(165, 259, 132, 20);
		contentPane.add(lblProvincia);
		
		JLabel lblNombreUsuario = new JLabel("Contrase�a");
		lblNombreUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombreUsuario.setBounds(165, 290, 91, 20);
		contentPane.add(lblNombreUsuario);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setBounds(165, 352, 113, 20);
		contentPane.add(btnAceptar);
		
		lblNewLabel = new JLabel("Confirmar contrase�a");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(165, 321, 141, 23);
		contentPane.add(lblNewLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(309, 290, 170, 20);
		contentPane.add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(309, 321, 170, 20);
		contentPane.add(passwordField_1);
		
		JLabel labelFondo = new JLabel("");
		labelFondo.setIcon(new ImageIcon(Crear_Usuario.class.getResource("/Documentos/Fondo.jpg")));
		labelFondo.setBounds(0, 0, 514, 396);
		contentPane.add(labelFondo);
		
		//EVENTOS
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					db.insertData4(txtCIF.getText(),txtNOMBRE.getText(),txtDIRECCION.getText(),Integer.parseInt(txtTELEFONO.getText()),txtEMAIL.getText(),
							txtLOCALIDAD.getText(),txtPROVINCIA.getText(),txtCUENTABANCARIA.getText(),passwordField.getPassword());
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
	}
}
