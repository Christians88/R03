package interficies;

import javax.swing.JFrame;

import SQL.*;
import interficies.ModificarVeterinarios;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Toolkit;

public class Apadrinamientos extends JFrame {
	
	private JTable table;
	private JButton btnModificar;
	private JTextField textField_CA;
	private String [][] apadrinamientos= new String [10][3];
	
	public Apadrinamientos() {
		setTitle("ShelterApp");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Apadrinamientos.class.getResource("/Documentos/Huella.png")));
		
		this.apadrinamientos[0][0]="DNI PADRINO";
		this.apadrinamientos[0][1]="CODIGO ANIMAL";
		this.apadrinamientos[0][2]="IMPORTE";
		
		for (int i=1;i<apadrinamientos.length;i++){
			for(int j=0;j<3;j++){
				this.apadrinamientos[i][j]="";
			}
			
		}
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 658, 426);
		getContentPane().setBackground(new Color(204, 204, 255));
		getContentPane().setLayout(null);
		setResizable(false);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(50, 11, 81, 108);
		lblNewLabel.setIcon(new ImageIcon(Apadrinamientos.class.getResource("/Documentos/Logo.png")));
		getContentPane().add(lblNewLabel);
		
		JLabel lblApadrinamientos = new JLabel("Apadrinamientos");
		lblApadrinamientos.setBounds(234, 58, 209, 27);
		lblApadrinamientos.setFont(new Font("Corbel", Font.BOLD, 22));
		getContentPane().add(lblApadrinamientos);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{apadrinamientos[0][0], apadrinamientos[0][1], apadrinamientos[0][2]},
				{apadrinamientos[1][0], apadrinamientos[1][1], apadrinamientos[1][2]}, 
				{apadrinamientos[2][0], apadrinamientos[2][1], apadrinamientos[2][2]}, 
				{apadrinamientos[3][0], apadrinamientos[3][1], apadrinamientos[3][2]}, 
				{apadrinamientos[4][0], apadrinamientos[4][1], apadrinamientos[4][2]}, 
				{apadrinamientos[5][0], apadrinamientos[5][1], apadrinamientos[5][2]}, 
				{apadrinamientos[6][0], apadrinamientos[6][1], apadrinamientos[6][2]}, 
				{apadrinamientos[7][0], apadrinamientos[7][1], apadrinamientos[7][2]}, 
				{apadrinamientos[8][0], apadrinamientos[8][1], apadrinamientos[8][2]},
			},
			new String[] {
				"DNI PADRINO", "CODIGO ANIMAL", "IMPORTE"
			}
			
		));
		table.setBounds(108, 137, 515, 144);
		getContentPane().add(table);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnActualizar.setBounds(260, 325, 145, 20);
		getContentPane().add(btnActualizar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnModificar.setBounds(436, 293, 145, 20);
		getContentPane().add(btnModificar);
		
		JButton btnAadir = new JButton("A\u00F1adir");
		btnAadir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAadir.setBounds(260, 293, 145, 20);
		getContentPane().add(btnAadir);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEliminar.setBounds(260, 357, 145, 20);
		getContentPane().add(btnEliminar);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon(Apadrinamientos.class.getResource("/Documentos/Fondo.jpg")));
		lblFondo.setBounds(0, 0, 658, 404);
		getContentPane().add(lblFondo);
		updateTable();
		
		//Boton Modificar
		
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int rows = table.getSelectedRow();
				String t1 = apadrinamientos[rows][0];
				String t2 = apadrinamientos[rows][1];
				String t3 = apadrinamientos[rows][2];
				ModificarApadrinamiento wa = new ModificarApadrinamiento(t1, t2, t3);
				
				
			}
		});
		
		//Boton Aņadir
		
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AņadirApadrinamientos wa = new AņadirApadrinamientos();
			}
		});
		
		//Boton Actualizar
		
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					apadrinamientos=db.getValues8();
					aņadirApadrinamientos(apadrinamientos,table);
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		//Boton eliminar
		
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				int row = table.getSelectedRow();
				String cm = apadrinamientos[row][1];
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.deleteRecord8("APADRINA", cm);
					db.closeConnection();
					updateTable();
					table.repaint();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
	
	
		//Metodos
			public void aņadirApadrinamientos(String[][] apadrinamientos, JTable table) {
				// TODO Auto-generated method stub
				table.setModel(new DefaultTableModel(
						new Object[][] {
							{apadrinamientos[0][0], apadrinamientos[0][1], apadrinamientos[0][2]},
							{apadrinamientos[1][0], apadrinamientos[1][1], apadrinamientos[1][2]}, 
							{apadrinamientos[2][0], apadrinamientos[2][1], apadrinamientos[2][2]}, 
							{apadrinamientos[3][0], apadrinamientos[3][1], apadrinamientos[3][2]}, 
							{apadrinamientos[4][0], apadrinamientos[4][1], apadrinamientos[4][2]}, 
							{apadrinamientos[5][0], apadrinamientos[5][1], apadrinamientos[5][2]}, 
							{apadrinamientos[6][0], apadrinamientos[6][1], apadrinamientos[6][2]}, 
							{apadrinamientos[7][0], apadrinamientos[7][1], apadrinamientos[7][2]}, 
							{apadrinamientos[8][0], apadrinamientos[8][1], apadrinamientos[8][2]},
						},
						new String[] {
							"DNI PADRINO", "CODIGO ANIMAL", "IMPORTE"
						}
					));	
			}
			
			private void updateTable(){
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					apadrinamientos=db.getValues8();
					aņadirApadrinamientos(apadrinamientos,table);
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
			
			public JButton getBtnModificar() {
				return btnModificar;
			}
			public void setBtnAadir(JButton btnAadir) {
				this.btnModificar = btnAadir;
			}


			public JTable getTable() {
				return table;
			}


			public void setTable(JTable table) {
				this.table = table;
			}


			public String[][] getApadrinamientos() {
				return apadrinamientos;
			}


			public void setApadrinamientos(String[][] apadrinamientos) {
				this.apadrinamientos = apadrinamientos;
			}
				
			
			}

		
