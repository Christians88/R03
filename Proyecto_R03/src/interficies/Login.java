package interficies;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import java.awt.SystemColor;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.EventObject;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.sql.*;
import SQL.*;
import javax.swing.SwingConstants;
public class Login extends JFrame {

	protected static final Crear_Usuario Crear_Usuario = null;
	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField passwordField;
	private JLabel lblContrasea;
	protected Object botonCambiar;
	protected EventObject evento;
	
	/**
	 * Create the frame.
	 */
	
	public Login() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Login.class.getResource("/Documentos/Huella.png")));
		setBackground(Color.DARK_GRAY);
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 429, 299);
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(Login.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(281, 6, 81, 108);
		contentPane.add(lblLogo);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(232, 140, 170, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(232, 172, 170, 20);
		contentPane.add(passwordField);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root","");
					if(db.log(txtUsuario.getText(),passwordField.getText())){
						Menu M=new Menu();
						M.setVisible(true);
						}
						else{
							JOptionPane.showMessageDialog(null,"CIF o contraseņa invalidos");
						}
						db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnEntrar.setForeground(SystemColor.menuText);
		btnEntrar.setBackground(Color.WHITE);
		btnEntrar.setBounds(127, 203, 89, 20);
		contentPane.add(btnEntrar);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsuario.setBounds(127, 141, 81, 20);
		contentPane.add(lblUsuario);
		
		JButton btnNU = new JButton("Nuevo Usuario");
		btnNU.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNU.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {     
					 
				Crear_Usuario CU=new Crear_Usuario();
				   CU.setVisible(true);
			
			}});
		btnNU.setForeground(SystemColor.menuText);
		btnNU.setBackground(Color.WHITE);
		btnNU.setBounds(158, 235, 170, 20);
		contentPane.add(btnNU);
		
		lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblContrasea.setBounds(127, 172, 71, 20);
		contentPane.add(lblContrasea);
		
		JLabel lblBienvenidoAShelter = new JLabel("Bienvenido a ");
		lblBienvenidoAShelter.setFont(new Font("Corbel", Font.BOLD, 22));
		lblBienvenidoAShelter.setBounds(127, 53, 235, 68);
		contentPane.add(lblBienvenidoAShelter);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon(Login.class.getResource("/Documentos/Fondo.jpg")));
		lblFondo.setBounds(0, 0, 429, 277);
		contentPane.add(lblFondo);
	
	

	
	
	
	
	
	
	}
}

