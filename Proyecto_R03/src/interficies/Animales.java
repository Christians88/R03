package interficies;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.Dimension;
import java.awt.Graphics;

import SQL.SQL;

import java.awt.Color;

public class Animales extends JFrame {

	private JPanel contentPane;
	private JTextField txtLogo;
	private ImageIcon imagen;
	private String nombre;
	/**
	 * Create the frame.
	 */
	public Animales(/*String nombre*/) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Animales.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 493, 422);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		this.nombre =nombre;
		// Logo de la empresa
		JLabel lblLogo = new JLabel("LOGO");
		lblLogo.setIcon(new ImageIcon(Animales.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(50, 11, 81, 108);
		contentPane.add(lblLogo);
		//Botón Adopciones
		JButton btnNewButton = new JButton("Adopciones");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(121, 196, 150, 20);
		contentPane.add(btnNewButton);
		//Botón Apadrinamientos
		JButton btnNewButton_1 = new JButton("Apadrinamientos");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setBounds(316, 194, 150, 20);
		contentPane.add(btnNewButton_1);
		//Botón Todos los Animales
		JButton btnNewButton_2 = new JButton("Todos los Animales");
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton_2.setBackground(Color.WHITE);
		btnNewButton_2.setBounds(316, 355, 150, 20);
		contentPane.add(btnNewButton_2);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Animales.class.getResource("/Documentos/Adopciones.png")));
		lblNewLabel.setBounds(148, 85, 100, 100);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Animales.class.getResource("/Documentos/Apadrinamientos.png")));
		lblNewLabel_1.setBounds(326, 85, 132, 100);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(Animales.class.getResource("/Documentos/TodoslosAnimales.png")));
		lblNewLabel_2.setBounds(326, 245, 132, 100);
		contentPane.add(lblNewLabel_2);
		
		JButton button = new JButton("Acogidas");
		button.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button.setBackground(Color.WHITE);
		button.setBounds(121, 357, 150, 20);
		contentPane.add(button);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon(Animales.class.getResource("/Documentos/Acogidas.png")));
		lblNewLabel_3.setBounds(134, 245, 108, 100);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblFondo = new JLabel("New label");
		lblFondo.setIcon(new ImageIcon(Animales.class.getResource("/Documentos/Fondo.jpg")));
		lblFondo.setBounds(0, 0, 493, 400);
		contentPane.add(lblFondo);
		
		//Eventos
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Apadrinamientos wa=new Apadrinamientos ();
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					wa.setApadrinamientos(db.getValues8());
					wa.a�adirApadrinamientos(wa.getApadrinamientos(), wa.getTable());
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				wa.setVisible(true);
			}
		});
		//Acogidas se actualiza solo al entrar
		button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						SQL db = new SQL();
						Acogidas wa = new Acogidas();
						try {
							db.SQLConnection("ShelterApp", "root", "");
							wa.setAcogidas(db.getValues3());
							wa.anadirAcogida(wa.getAcogidas(), wa.getTable());
							db.closeConnection();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
		// Todos los animales se actualizan solos al entrar
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				TodoslosAnimales wa = new TodoslosAnimales();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					wa.setAnimales(db.getValues2());
					wa.anadirAnimal(wa.getAnimales(), wa.getTable());
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		//Apadrinamientos se actualizan solo al entrar
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				Apadrinamientos wa = new Apadrinamientos();
				try {
				db.SQLConnection("ShelterApp", "root", "");
					wa.setApadrinamientos(db.getValues8());
					wa.a�adirApadrinamientos(wa.getApadrinamientos(),wa.getTable());
					
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		// Adopciones se actualizan solos al entrar
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				Adopciones wa = new Adopciones();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					wa.setAdopciones(db.getValues());
					wa.a�adirAdopcion(wa.getAdopciones(), wa.getTable());
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
	/*
	//METODOS
	//IMAGEN DE FONDO
	public void rellenaFondo(Graphics g){
		Dimension tama�o=getSize();
		imagen = new ImageIcon(getClass().getResource(nombre));
		g.drawImage(imagen.getImage(),0,0, tama�o.width,tama�o.height,null);
		this.contentPane.setOpaque(false);
		super.paint(g);
	}*/
} 
