package interficies;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import SQL.SQL;

public class Veterinarios extends JFrame {

	private JPanel contentPane;
	private JButton btnAadir;
	private JTable table;
	private String[][] veterinario = new String[10][5];

	/**
	 * Create the frame.
	 */
	public Veterinarios() {

		this.veterinario[0][0]="DNI";
		this.veterinario[0][1]="CIF_PERRERA";
		this.veterinario[0][2]="NOMBRE";
		this.veterinario[0][3]="APELLIDO1";
		this.veterinario[0][4]="APELLIDO2";
		for (int i=1;i<veterinario.length;i++){
			for(int j=0;j<5;j++){
				this.veterinario[i][j]="";
			}
			
		}

		setIconImage(Toolkit.getDefaultToolkit().getImage(Veterinarios.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 661, 457);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setVisible(true);
		// Boton de aņadir
		btnAadir = new JButton("A\u00F1adir");
		btnAadir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAadir.setBounds(265, 335, 140, 20);
		contentPane.add(btnAadir);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{veterinario[0][0], veterinario[0][1], veterinario[0][2], veterinario[0][3], veterinario[0][4]},
				{veterinario[1][0], veterinario[1][1], veterinario[1][2], veterinario[1][3], veterinario[1][4]},
				{veterinario[2][0], veterinario[2][1], veterinario[2][2], veterinario[2][3], veterinario[2][4]},
				{veterinario[3][0], veterinario[3][1], veterinario[3][2], veterinario[3][3], veterinario[3][4]},
				{veterinario[4][0], veterinario[4][1], veterinario[4][2], veterinario[4][3], veterinario[4][4]},
				{veterinario[5][0], veterinario[5][1], veterinario[5][2], veterinario[5][3], veterinario[5][4]},
				{veterinario[6][0], veterinario[6][1], veterinario[6][2], veterinario[6][3], veterinario[6][4]},
				{veterinario[7][0], veterinario[7][1], veterinario[7][2], veterinario[7][3], veterinario[7][4]},
				{veterinario[8][0], veterinario[8][1], veterinario[8][2], veterinario[8][3], veterinario[8][4]},
			},
			new String[] {
				"DNI", "CIF_PERRERA", "NOMBRE", "APELLIDO1", "APELLIDO2"
			}
		));
		table.setBounds(83, 164, 544, 144);
		contentPane.add(table);
		
		JLabel lblVeterinarios = new JLabel("Veterinarios");
		lblVeterinarios.setFont(new Font("Corbel", Font.BOLD, 22));
		lblVeterinarios.setBounds(276, 96, 145, 23);
		contentPane.add(lblVeterinarios);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Veterinarios.class.getResource("/Documentos/Logo.png")));
		lblNewLabel.setBounds(50, 11, 81, 108);
		contentPane.add(lblNewLabel);
		
		// Boton actualizar la info de la tabla
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnActualizar.setBounds(265, 366, 140, 20);
		contentPane.add(btnActualizar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				int row = table.getSelectedRow();
				String cm = veterinario[row][0];
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.deleteRecordtesting(cm);
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnEliminar.setBounds(265, 397, 140, 20);
		contentPane.add(btnEliminar);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					int rows = table.getSelectedRow();
					String t1 = veterinario[rows][0];
					String t2 = veterinario[rows][1];
					String t3 = veterinario[rows][2];
					String t4 = veterinario[rows][3];
					String t5 = veterinario[rows][4];
					ModificarVeterinarios wa = new ModificarVeterinarios(t1, t2, t3,t4,t5);
			}
		});
		btnEditar.setBounds(420, 335, 89, 20);
		contentPane.add(btnEditar);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(Veterinarios.class.getResource("/Documentos/Fondo.jpg")));
		lblNewLabel_1.setBounds(0, 0, 709, 435);
		contentPane.add(lblNewLabel_1);
		// Eventos
		// Avanzar hasta la ventana de modificar
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AņadirVeterinarios wa = new AņadirVeterinarios();
				wa.setVisible(true);
			}
		});
		// Actualizar la info de la tabla
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					veterinario=db.getValues1();
					aņadirVeterinarios(veterinario,table);
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
	//Metodos
		public void aņadirVeterinarios(String[][] array,JTable table){
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{veterinario[0][0], veterinario[0][1], veterinario[0][2], veterinario[0][3], veterinario[0][4]},
					{veterinario[1][0], veterinario[1][1], veterinario[1][2], veterinario[1][3], veterinario[1][4]},
					{veterinario[2][0], veterinario[2][1], veterinario[2][2], veterinario[2][3], veterinario[2][4]},
					{veterinario[3][0], veterinario[3][1], veterinario[3][2], veterinario[3][3], veterinario[3][4]},
					{veterinario[4][0], veterinario[4][1], veterinario[4][2], veterinario[4][3], veterinario[4][4]},
					{veterinario[5][0], veterinario[5][1], veterinario[5][2], veterinario[5][3], veterinario[5][4]},
					{veterinario[6][0], veterinario[6][1], veterinario[6][2], veterinario[6][3], veterinario[6][4]},
					{veterinario[7][0], veterinario[7][1], veterinario[7][2], veterinario[7][3], veterinario[7][4]},
					{veterinario[8][0], veterinario[8][1], veterinario[8][2], veterinario[8][3], veterinario[8][4]},
				},
				new String[] {
					"DNI", "CIF_PERRERA", "NOMBRE", "APELLIDO1", "APELLIDO2"
				}
			));	
	}
		public JButton getBtnAadir() {
			return btnAadir;
		}
		public void setBtnAadir(JButton btnAadir) {
			this.btnAadir = btnAadir;
		}
		public JTable getTable() {
			return table;
		}
		public void setTable(JTable table) {
			this.table = table;
		}
		public String[][] getVeterinario() {
			return veterinario;
		}
		public void setVeterinario(String[][] veterinario) {
			this.veterinario = veterinario;
		}
}
