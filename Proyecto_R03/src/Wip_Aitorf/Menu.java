package Wip_Aitorf;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import interficies.Animales;
import interficies.Informar;
import interficies.Veterinarios;

import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JEditorPane;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Color;

public class Menu extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Menu() {
		
		/*model.insterRow(con,new Object[]{});
		model.setValeuAt(p.getDI(),con,0);
		model.setValeuAt(p.getCIF(),con,1);
		model.setValeuAt(p.getNOMBRE(),con,2);
		model.setValeuAt(p.getDIRECCION(),con,3);
		model.setValeuAt(p.getTELEFONO(),con,4);
		model.setValeuAt(p.getCORREOELECTRONICO(),con,5);
		model.setValeuAt(p.getLOCALIDAD(),con,6);
		model.setValeuAt(p.getPROVINCIA(),con,7);
		model.setValeuAt(p.getNUMERODECUENTA(),con,8);
		con++;*/
		
		
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Huella.png")));
		setTitle("BIENVENIDOS A SHELTER app");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 689, 485);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setForeground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnInformes = new JButton("Informar");
		btnInformes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Informar I=new Informar();
				I.setVisible(true);
			}
		});
		btnInformes.setBackground(Color.WHITE);
		btnInformes.setBounds(100, 150, 85, 23);
		contentPane.add(btnInformes);
		
		JButton btnEditarPerfil = new JButton("Editar Perfil");
		btnEditarPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Crear_Usuario CU = new Crear_Usuario();
				CU.setVisible(true);
				
			}
		});
		btnEditarPerfil.setBackground(Color.WHITE);
		btnEditarPerfil.setBounds(478, 150, 89, 23);
		contentPane.add(btnEditarPerfil);
		
		JButton btnAnimales = new JButton("Animales");
		btnAnimales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Animales A=new Animales();
				A.setVisible(true);
			}
		});
		btnAnimales.setBackground(Color.WHITE);
		btnAnimales.setBounds(100, 313, 85, 23);
		contentPane.add(btnAnimales);
		
		JButton btnVeterinario = new JButton("Veterinario");
		btnVeterinario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Veterinarios V= new Veterinarios();
				V.setVisible(true);
			}
		});
		btnVeterinario.setBackground(Color.WHITE);
		btnVeterinario.setBounds(478, 313, 89, 23);
		contentPane.add(btnVeterinario);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBounds(478, 213, 85, 62);
		contentPane.add(editorPane);
		
		JEditorPane editorPane_1 = new JEditorPane();
		editorPane_1.setBounds(100, 213, 85, 62);
		contentPane.add(editorPane_1);
		
		JEditorPane editorPane_2 = new JEditorPane();
		editorPane_2.setBounds(100, 53, 85, 62);
		contentPane.add(editorPane_2);
		
		JEditorPane editorPane_3 = new JEditorPane();
		editorPane_3.setBounds(478, 53, 85, 62);
		contentPane.add(editorPane_3);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(289, 11, 85, 130);
		contentPane.add(lblLogo);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/Huella.png")));
		label.setBounds(380, 150, 61, 59);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/Huella.png")));
		label_1.setBounds(182, 365, 61, 53);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/Huella.png")));
		label_2.setBounds(257, 235, 66, 53);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/Huella.png")));
		label_3.setBounds(368, 306, 61, 59);
		contentPane.add(label_3);
		
		JButton btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login L= new Login();
				L.setVisible(true);
			}
		});
		btnAtras.setBackground(Color.WHITE);
		btnAtras.setBounds(272, 387, 89, 23);
		contentPane.add(btnAtras);
	

	
	
	
	
	}
}
