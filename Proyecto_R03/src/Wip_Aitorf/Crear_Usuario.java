package Wip_Aitorf;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JEditorPane;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import java.sql.SQLException;

public class Crear_Usuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtCIF;
	private JTextField txtNOMBRE;
	private JTextField txtDIRECCION;
	private JTextField txtTELEFONO;
	private JTextField txtEMAIL;
	private JTextField txtLOCALIDAD;
	private JTextField txtPROVINCIA;
	private JTextField txtNUMEROCUENTA;
	private JTextField txtNOMBREUSUARIO;
	private JLabel lblNewLabel;
	private JPasswordField passwordField;
	private JLabel lblLogo;
	private JLabel lblPies;
	private JLabel label;
	private JLabel label_1;

	/**
	 * Create the frame.
	 */
	public Crear_Usuario() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Crear_Usuario.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 689, 485);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtCIF = new JTextField();
		txtCIF.setBounds(238, 44, 91, 20);
		contentPane.add(txtCIF);
		txtCIF.setColumns(10);
		
		txtNOMBRE = new JTextField();
		txtNOMBRE.setBounds(238, 75, 91, 20);
		contentPane.add(txtNOMBRE);
		txtNOMBRE.setColumns(10);
		
		txtDIRECCION = new JTextField();
		txtDIRECCION.setBounds(238, 106, 91, 20);
		contentPane.add(txtDIRECCION);
		txtDIRECCION.setColumns(10);
		
		txtTELEFONO = new JTextField();
		txtTELEFONO.setBounds(238, 199, 91, 20);
		contentPane.add(txtTELEFONO);
		txtTELEFONO.setColumns(10);
		
		txtEMAIL = new JTextField();
		txtEMAIL.setBounds(238, 168, 91, 20);
		contentPane.add(txtEMAIL);
		txtEMAIL.setColumns(10);
		
		txtLOCALIDAD = new JTextField();
		txtLOCALIDAD.setBounds(238, 137, 91, 20);
		contentPane.add(txtLOCALIDAD);
		txtLOCALIDAD.setColumns(10);
		
		txtPROVINCIA = new JTextField();
		txtPROVINCIA.setBounds(238, 236, 91, 20);
		contentPane.add(txtPROVINCIA);
		txtPROVINCIA.setColumns(10);
		
		txtNUMEROCUENTA = new JTextField();
		txtNUMEROCUENTA.setBounds(238, 267, 91, 20);
		contentPane.add(txtNUMEROCUENTA);
		txtNUMEROCUENTA.setColumns(10);
		
		txtNOMBREUSUARIO = new JTextField();
		txtNOMBREUSUARIO.setBounds(238, 298, 91, 20);
		contentPane.add(txtNOMBREUSUARIO);
		txtNOMBREUSUARIO.setColumns(10);
		
		JLabel lblNombre = new JLabel("CIF");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(87, 44, 50, 20);
		contentPane.add(lblNombre);
		
		JLabel lblPrimerApellido = new JLabel("NOMBRE");
		lblPrimerApellido.setBounds(87, 75, 86, 20);
		contentPane.add(lblPrimerApellido);
		
		JLabel lblSegundoApelllido = new JLabel("DIRECCION");
		lblSegundoApelllido.setBounds(87, 106, 91, 20);
		contentPane.add(lblSegundoApelllido);
		
		JLabel lblDni = new JLabel("LOCALIDAD");
		lblDni.setBounds(87, 137, 71, 20);
		contentPane.add(lblDni);
		
		JLabel lblNombreAnimales = new JLabel("EMAIL");
		lblNombreAnimales.setBounds(87, 168, 91, 20);
		contentPane.add(lblNombreAnimales);
		
		JLabel lblTipoDeAnimal = new JLabel("TELEFONO");
		lblTipoDeAnimal.setBounds(87, 199, 91, 20);
		contentPane.add(lblTipoDeAnimal);
		
		JLabel lblLocalidad = new JLabel("PROVINCIA");
		lblLocalidad.setBounds(87, 236, 91, 20);
		contentPane.add(lblLocalidad);
		
		JLabel lblProvincia = new JLabel("NUMERO DE CUENTA");
		lblProvincia.setBounds(87, 267, 107, 20);
		contentPane.add(lblProvincia);
		
		JLabel lblNombreUsuario = new JLabel("NOMBRE USUARIO");
		lblNombreUsuario.setBounds(87, 298, 91, 20);
		contentPane.add(lblNombreUsuario);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try { 
				
				SQL miconexion= new SQL();
				String sentenciaInsert="insert into values perrera(?,?,?,?,?,?,?,?)";
				miconexion.psPrepararSentencias=miconexion.miConexion.prepareStatement(sentenciaInsert);
				miconexion.psPrepararSentencias.setString(1,txtCIF.getText());
				miconexion.psPrepararSentencias.setString(2,txtNOMBRE.getText());
				miconexion.psPrepararSentencias.setString(3,txtDIRECCION.getText());
				miconexion.psPrepararSentencias.setString(4,txtLOCALIDAD.getText());
				miconexion.psPrepararSentencias.setString(5,txtEMAIL.getText());
				miconexion.psPrepararSentencias.setString(6,txtTELEFONO.getText());
				miconexion.psPrepararSentencias.setString(7,txtPROVINCIA.getText());
				miconexion.psPrepararSentencias.executeUpdate();
				
				
		}catch (Exception e2){
			System.out.println(e2.getCause());
		}
		}
	});
	btnAceptar.setBackground(SystemColor.menuText);
	btnAceptar.setBounds(514, 392, 86, 23);
	contentPane.add(btnAceptar);
		
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setBounds(514, 392, 86, 23);
		contentPane.add(btnAceptar);
		
		lblNewLabel = new JLabel("CONTRASE\u00D1A");
		lblNewLabel.setBounds(87, 329, 86, 23);
		contentPane.add(lblNewLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(238, 329, 91, 20);
		contentPane.add(passwordField);
		
		JButton btnNewButton = new JButton("Restablecer");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(240, 392, 89, 23);
		contentPane.add(btnNewButton);
		
		lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(Crear_Usuario.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(514, 25, 107, 150);
		contentPane.add(lblLogo);
		
		lblPies = new JLabel("");
		lblPies.setIcon(new ImageIcon(Crear_Usuario.class.getResource("/Documentos/Huella.png")));
		lblPies.setBounds(368, 280, 71, 57);
		contentPane.add(lblPies);
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(Crear_Usuario.class.getResource("/Documentos/Huella.png")));
		label.setBounds(414, 156, 61, 63);
		contentPane.add(label);
		
		label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Crear_Usuario.class.getResource("/Documentos/Huella.png")));
		label_1.setBounds(554, 222, 67, 57);
		contentPane.add(label_1);
		
		JLabel lblCrearUsuario = new JLabel("Crear usuario");
		lblCrearUsuario.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCrearUsuario.setBounds(290, 6, 148, 23);
		contentPane.add(lblCrearUsuario);
	}
}
