package WIP_Christian;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import SQL.SQL;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class Acogidas extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private String[][] acogidas = new String[10][3];

	/**
	 * Create the frame.
	 */
	public Acogidas() {
		this.acogidas[0][0]="Codigo Mascota";
		this.acogidas[0][1]="DNI Adoptante";
		this.acogidas[0][2]="Tiempo";
		for (int i=1;i<acogidas.length;i++){
			for(int j=0;j<3;j++){
				this.acogidas[i][j]="";
			}
			
		}
		setIconImage(Toolkit.getDefaultToolkit().getImage(Acogidas.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 689, 485);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setVisible(true);
		
		JLabel lblLogo = new JLabel("LOGO");
		lblLogo.setIcon(new ImageIcon(Acogidas.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(10, 11, 81, 108);
		contentPane.add(lblLogo);
		//Tabla

		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{acogidas[0][0],acogidas[0][1],acogidas[0][2]},
					{acogidas[1][0],acogidas[1][1],acogidas[1][2]},
					{acogidas[2][0],acogidas[2][1],acogidas[2][2]},
					{acogidas[3][0],acogidas[3][1],acogidas[3][2]},
					{acogidas[4][0],acogidas[4][1],acogidas[4][2]},
					{acogidas[5][0],acogidas[5][1],acogidas[5][2]},
					{acogidas[6][0],acogidas[6][1],acogidas[6][2]},
					{acogidas[7][0],acogidas[7][1],acogidas[7][2]},
					{acogidas[8][0],acogidas[8][1],acogidas[8][2]},
				},
				new String[] {
					"Codigo", "DNI Adoptante", "Tiempo"
				}
			));
		table.setBounds(225, 85, 361, 200);
		contentPane.add(table);
		//Boton A�adir
		JButton btnNewButton = new JButton("A�adir/Editar");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(278, 256, 151, 23);
		contentPane.add(btnNewButton);
		//Boton Editar
		JButton btnNewButton_1 = new JButton("Actualizar");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setBounds(278, 303, 151, 23);
		contentPane.add(btnNewButton_1);
		
		JLabel lblApadrinamientos = new JLabel("Acogidas");
		lblApadrinamientos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblApadrinamientos.setBounds(319, 52, 81, 23);
		contentPane.add(lblApadrinamientos);
		
		//Eventos
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarAcogida ma=new ModificarAcogida();
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				try {
					db.SQLConnection("ShelterApp", "root", "");
					acogidas=db.getValues4();
					anadirAcogida(acogidas,table);
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
	//METODOS
			public void anadirAcogida(String[][] array,JTable table){
				table.setModel(new DefaultTableModel(
						new Object[][] {
							{acogidas[0][0],acogidas[0][1],acogidas[0][2]},
							{acogidas[1][0],acogidas[1][1],acogidas[1][2]},
							{acogidas[2][0],acogidas[2][1],acogidas[2][2]},
							{acogidas[3][0],acogidas[3][1],acogidas[3][2]},
							{acogidas[4][0],acogidas[4][1],acogidas[4][2]},
							{acogidas[5][0],acogidas[5][1],acogidas[5][2]},
							{acogidas[6][0],acogidas[6][1],acogidas[6][2]},
							{acogidas[7][0],acogidas[7][1],acogidas[7][2]},
							{acogidas[8][0],acogidas[8][1],acogidas[8][2]},
						},
						new String[] {
							"Codigo", "DNI", "Tiempo"
						}
					));
			}
			public void eliminarAcogida(String[][] array,JTable table){
					int a=5;
						for(int i=1;i<array.length;i++){
							for(int j=0;i<3;j++){
								if(array[i][j].equals("")){
									a=i;
								}
							}
						}
						acogidas[a][0]="";
						acogidas[a][1]="";
						acogidas[a][2]="";
				table.setModel(new DefaultTableModel(
						new Object[][] {
							{acogidas[0][0],acogidas[0][1],acogidas[0][2]},
							{acogidas[1][0],acogidas[1][1],acogidas[1][2]},
							{acogidas[2][0],acogidas[2][1],acogidas[2][2]},
							{acogidas[3][0],acogidas[3][1],acogidas[3][2]},
							{acogidas[4][0],acogidas[4][1],acogidas[4][2]},
							{acogidas[5][0],acogidas[5][1],acogidas[5][2]},
							{acogidas[6][0],acogidas[6][1],acogidas[6][2]},
							{acogidas[7][0],acogidas[7][1],acogidas[7][2]},
							{acogidas[8][0],acogidas[8][1],acogidas[8][2]},
						},
						new String[] {
							"Codigo", "DNI", "Tiempo"
						}
					));
			}
			public JTable getTable4() {
				return table;
			}
			public void setTable(JTable table) {
				this.table = table;
			}
			public String[][] getAcogidas() {
				return acogidas;
			}
			public void setAdopciones() {
				this.acogidas = acogidas;
			}
			
	}


