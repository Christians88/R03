package WIP_Christian;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class SQL {
	private static Connection Conexion = null;

	//MÉTODO PERSONALIZADO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        }
	    }
         
	//MÉTODO QUE FINALIZA LA CONEXION A SQL
	public void closeConnection() {
	        try {
	            Conexion.close();
	            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexión con el servidor");
	        } catch (SQLException ex) {
	           
	            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexión con el servidor");
	        }
	    }
    
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
	 public void createTable(String name) {
	        try {
	            String Query = "CREATE TABLE " + name + ""
	                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
	                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	 
	 //METODO QUE ELIMINA TABLA EN NUESTRA BASE DE DATOS
	 public void deleteTable(String name) {
		try{
			String Query = "DROP TABLE " + name + " cascade constraints";
			Statement st = Conexion.createStatement();
	        st.executeUpdate(Query);
	        JOptionPane.showMessageDialog(null, "Se ha eliminado la tabla " + name + " de forma exitosa");
		} catch (SQLException ex){
			Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		}
	 }
	 		
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, String ID) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE DI = " + ID ;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla eliminada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }
	
	//METODO QUE EDITA VALORES DEL STIPO STRING DE NUESTRA BASE DE DATOS
	 
	 public void editSTRINGRecord(String table_name, String ID, String campo, String nuevo_valor ){
		 try {
	            String Query = "UPDATE " + table_name + " SET " + campo + " = " + nuevo_valor + " where DI = " + ID;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla editada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	        }
	 }
	 
	//METODO QUE EDITA VALORES DEL STIPO INT DE NUESTRA BASE DE DATOS
	 
	 public void editINTRecord(String table_name, String ID, int campo, String nuevo_valor ){
		 try {
	            String Query = "UPDATE " + table_name + " SET " + campo + "=" + nuevo_valor + " where DI = " + ID;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla editada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
	        }
	 }
	 
	 //METODOS PERSONALIZADOS PARA CADA TABLA
	 //TABLA ACOGIDA
	//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertData(String table_name, String codigo, String DNI, String tiempo) {
		        try {
		        	int maxID = 0;
		        	Statement s2 = Conexion.createStatement();
		        	s2.execute("SELECT MAX(DI) FROM " + table_name );    
		        	ResultSet rs2 = s2.getResultSet(); // 
		        	if ( rs2.next() ){
		        	  maxID = rs2.getInt(1);
		        	}
		        	
		        	s2.close();
		        	maxID = maxID + 1;
		        	
		            String Query = "INSERT INTO " + table_name + " (di, codigo, DNI, tiempo)"
		            		+ " VALUES ("
		            		+ maxID +","
		            		+ "'"+ codigo + "',"
		            		+ "'"+ DNI + "',"
		            		+ ""+ tiempo + ")";
		            		
		            		   		
		            Statement st = Conexion.createStatement();
		            //st.executeUpdate(Query);
		            st.execute(Query);
		            //st.executeQuery(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
	
		//METODO QUE EDITA ACOGIDA
		 public void editAcogida(int ID, String nuevo_valor ){
			 try {   
				 String Query = "UPDATE " + "MASCOTAS" + " SET " + "DNI_DUE�O" + " = " + "'" + nuevo_valor + "'" + " WHERE DI = " + ID;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Tupla editada");

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		 }
		 
		 //METODO QUE ELIMINA ACOGIDA
		 public void eliminaAcogida(int ID){
			 try {   
				 String Query = "UPDATE " + "MASCOTAS" + " SET DNI DUEÑO = NULL WHERE DI = " + ID;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Acogida eliminada");
		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		 }
		 
		//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
			public String[][] getValues4() {
				
				
				String array[][]=new String[10][3];
				array[0][0]="CODIGO_MASCOTA";
				array[0][1]="DNI";
				array[0][2]="TIEMPO";
				for (int i=1;i<array.length;i++){
					for(int j=0;j<3;j++){
						array[i][j]="";
					}
					
				}
				int i=1;
			   try {
			            String Query = "SELECT CODIGO, DNI, TIEMPO FROM ACOGIDA";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			           while (resultSet.next()) {
			                        array[i][0]= resultSet.getString("CODIGO_MASCOTA");
			                        array[i][1]= resultSet.getString("DNI");
			                        array[i][2]= resultSet.getString("TIEMPO");
			                        i++;
			            }
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
			        }
			   	return(array);
			    }
}
