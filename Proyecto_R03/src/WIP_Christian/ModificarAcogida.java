package WIP_Christian;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import SQL.SQL;

import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.Font;

public class ModificarAcogida extends JFrame {

	private JPanel contentPane;
	private JTextField textDNI;
	private JTextField textCodigo;
	private JTextField textField_2;
	/**
	 * Create the frame.
	 */
	public ModificarAcogida() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ModificarAcogida.class.getResource("/Documentos/Huella.png")));
		setTitle("ShelterApp");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 689, 485);
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setVisible(true);
		
		
		JLabel lblLogo = new JLabel("LOGO");
		lblLogo.setIcon(new ImageIcon(ModificarAcogida.class.getResource("/Documentos/Logo.png")));
		lblLogo.setBounds(10, 11, 81, 108);
		contentPane.add(lblLogo);
		
		textDNI = new JTextField();
		textDNI.setBounds(307, 221, 92, 20);
		contentPane.add(textDNI);
		textDNI.setColumns(10);
		
		JButton btnAadir = new JButton("Añadir/Editar");
		btnAadir.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAadir.setBackground(Color.WHITE);
		btnAadir.setBounds(546, 365, 117, 29);
		contentPane.add(btnAadir);
		
		JLabel lblTiempoAcogida = new JLabel("Tiempo Acogida");
		lblTiempoAcogida.setBounds(168, 271, 92, 14);
		contentPane.add(lblTiempoAcogida);
		
		JButton button_1 = new JButton("Eliminar");
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button_1.setBackground(Color.WHITE);
		button_1.setBounds(546, 406, 117, 29);
		contentPane.add(button_1);
		
		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setBounds(168, 223, 92, 14);
		contentPane.add(lblDNI);
		
		JLabel lblCodigo = new JLabel("Código Animal");
		lblCodigo.setBounds(168, 171, 92, 14);
		contentPane.add(lblCodigo);
		
		textCodigo = new JTextField();
		textCodigo.setColumns(10);
		textCodigo.setBounds(307, 165, 92, 20);
		contentPane.add(textCodigo);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(307, 271, 92, 20);
		contentPane.add(textField_2);
		

		//EVENTOS
		//A�ADIR/EDITAR
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 SQL db = new SQL();
				 try {
					 db.SQLConnection("ShelterApp", "root", "");
					 db.editAcogida(Integer.parseInt(textCodigo.getText()),textDNI.getText(),textField_2.getText());
					 db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
			}
		});
		//ELIMINAR
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQL db = new SQL();
				 try {
					db.SQLConnection("ShelterApp", "root", "");
					db.eliminaAcogida(Integer.parseInt(textCodigo.getText()));
					db.closeConnection();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
}
