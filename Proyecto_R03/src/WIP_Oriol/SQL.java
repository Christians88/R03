package WIP_Oriol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class SQL {
	private static Connection Conexion = null;

	//MÉTODO PERSONALIZADO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
        }
    }
     
//MÉTODO QUE FINALIZA LA CONEXION A SQL
public void closeConnection() {
        try {
            Conexion.close();
            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexión con el servidor");
        } catch (SQLException ex) {
           
            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexión con el servidor");
        }
    }

//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
 public void createTable(String name) {
        try {
            String Query = "CREATE TABLE " + name + ""
                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

            Statement st = Conexion.createStatement();
            st.executeUpdate(Query);
            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
        } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
 //METODO QUE ELIMINA TABLA EN NUESTRA BASE DE DATOS
 public void deleteTable(String name) {
	try{
		String Query = "DROP TABLE " + name + " cascade constraints";
		Statement st = Conexion.createStatement();
        st.executeUpdate(Query);
        JOptionPane.showMessageDialog(null, "Se ha eliminado la tabla " + name + " de forma exitosa");
	} catch (SQLException ex){
		Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	}
 }
 		
//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, int ID) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE DI = " + ID ;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Tupla eliminada");

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }

//METODO QUE EDITA VALORES DEL STIPO STRING DE NUESTRA BASE DE DATOS
 
 public void editSTRINGRecord(String table_name, int ID, String campo, String nuevo_valor ){
	 try {
            String Query = "UPDATE " + table_name + " SET " + campo + " = " + "'" + nuevo_valor + "'" + " where DI = " + ID;
            Statement st = Conexion.createStatement();
            st.executeUpdate(Query);
            JOptionPane.showMessageDialog(null, "Tupla editada");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
        }
 }
 
//METODO QUE EDITA VALORES DEL STIPO INT DE NUESTRA BASE DE DATOS
 
 public void editINTRecord(String table_name, int ID, int campo, String nuevo_valor ){
	 try {
            String Query = "UPDATE " + table_name + " SET " + campo + "=" + nuevo_valor + " where DI = " + ID;
            Statement st = Conexion.createStatement();
            st.executeUpdate(Query);
            JOptionPane.showMessageDialog(null, "Tupla editada");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
        }
 }	
				


			//TABLA Veterinarios-----------------------------------------
			// A�adir veterinarios
	public void insertData1(String table_name, String DNI, String CIF_PROTECTORA, String NOMBRE, String APELLIDO1, String APELLIDO2) {
       try {
           String Query = "INSERT INTO " + "VETERINARIO" +
           		" VALUES("
           		+ "di_seq3.NEXTVAL,"
           		+ "'"+ DNI + "',"
           		+ "'"+ CIF_PROTECTORA + "',"
           		+ "'"+ NOMBRE + "',"
           		+ "'"+ APELLIDO1 + "',"
           		+ "'"+ APELLIDO2 + "',";
           		   		
           Statement st = Conexion.createStatement();
           st.executeUpdate(Query);
           JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
       }
	}
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
	public void getValues1(String table_name) {
	   try {
	            String Query = "SELECT DNI,CIF_PERRERA,NOMBRE,APELLIDO1,APELLIDO2 FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println(
	                        "DNI: " + resultSet.getString("DNI") + " " 
	                        + "CIF_PERRERA: " + resultSet.getString("CIF_PERRERA") + " "
	                        + "NOMBRE: " + resultSet.getString("NOMBRE") + " "
	                        + "APELLIDO1: " + resultSet.getString("APELLIDO1") + " "
	                        + "APELLIDO2: " + resultSet.getString("APELLIDO2"));
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
	        }
	}
		//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		public String[][]getValues() {
			
			String array[][]=new String[10][5];
			array[0][0]="DNI";
			array[0][1]="CIF_PERRERA";
			array[0][2]="NOMBRE";
			array[0][3]="APELLIDO1";
			array[0][4]="APELLIDO2";
			for (int i=0;i<array.length;i++){
				for(int j=1;j<5;j++){
					array[i][j]="";
				}
				
			}
			int i=1;
		   try {
		            String Query = "SELECT DNI,CIF_PERRERA,NOMBRE,APELLIDO1,APELLIDO2 FROM VETERINARIO";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		           while (resultSet.next()) {
		                        array[i][0]= resultSet.getString("DNI");
		                        array[i][1]= resultSet.getString("CIF_PERRERA");
		                        array[i][2]= resultSet.getString("NOMBRE");
		                        array[i][3]= resultSet.getString("APELLIDO1");
		                        array[i][4]= resultSet.getString("APELLIDO2");
		                        i++;
		            }
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisicion de datos");
		        }
		   	return(array);
		    }
}
		